// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
//
//  DependenciesInjection.h
//  TousAntiCovid
//
//  Created by Lunabee Studio / Date - 08/02/2023 - for the TousAntiCovid project.
//

#import <Foundation/Foundation.h>

//! Project version number for DependenciesInjection.
FOUNDATION_EXPORT double DependenciesInjectionVersionNumber;

//! Project version string for DependenciesInjection.
FOUNDATION_EXPORT const unsigned char DependenciesInjectionVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <DependenciesInjection/PublicHeader.h>


