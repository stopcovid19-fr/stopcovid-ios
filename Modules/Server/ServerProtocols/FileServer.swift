// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
//
//  FileServer.swift
//  TousAntiCovid
//
//  Created by Lunabee Studio / Date - 27/02/2023 - for the TousAntiCovid project.
//

public protocol FileServer {
    associatedtype Model: Decodable
    var fileUrl: URL { get }
    func fetchData(force: Bool, _ completion: @escaping (Result<Model?, Error>) -> ())
}

extension FileServer {
    public func fetchData(force: Bool, _ completion: @escaping (Result<Model?, Error>) -> ()) {
        let dataTask: URLSessionDataTask = UrlSessionManager.shared.dataTaskWithETag(with: fileUrl, resetEtag: force) { data, response, error, updateEtag in
            if let data {
                do {
                    let object: Model = try JSONDecoder().decode(Model.self, from: data)
                    updateEtag()
                    completion(.success(object))
                } catch {
                    completion(.failure(error))
                }
            } else if let error {
                completion(.failure(error))
            } else {
                // Etag: File not modified
                completion(.success(nil))
            }
        }
        dataTask.resume()
    }
}
