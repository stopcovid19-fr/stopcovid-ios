// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
//
//  InfoCenterServer.swift
//  TousAntiCovid
//
//  Created by Lunabee Studio / Date - 01/02/2023 - for the TousAntiCovid project.
//

public enum InfoCenterServer {
    public static func fetchTagsFile(_ completion: @escaping (Result<[RESTInfoTag], Error>) -> ()) {
        let dataTask: URLSessionDataTask = UrlSessionManager.shared.dataTaskWithETag(with: Constant.InfoCenter.tagsUrl) { data, response, error, updateEtag in
            guard let data else {
                completion(.failure(RESTError.noData))
                return
            }
            do {
                let tags: [RESTInfoTag] = try JSONDecoder().decode([RESTInfoTag].self, from: data)
                updateEtag()
                completion(.success(tags))
            } catch {
                completion(.failure(error))
            }
        }
        dataTask.resume()
    }
    
    public static func fetchInfoCenterFile(_ completion: @escaping (Result<[RESTInfo], Error>) -> ()) {
        let dataTask: URLSessionDataTask = UrlSessionManager.shared.dataTaskWithETag(with: Constant.InfoCenter.infoCenterUrl) { data, response, error, updateEtag in
            guard let data else {
                completion(.failure(RESTError.noData))
                return
            }
            do {
                let infos: [RESTInfo] = try JSONDecoder().decode([RESTInfo].self, from: data)
                updateEtag()
                completion(.success(infos))
            } catch {
                completion(.failure(error))
            }
        }
        dataTask.resume()
    }
    
    
    public static func fetchLabelsFile(languageCode: String, completion: @escaping (Result<[String: String], Error>) -> ()) {
        let url: URL = labelsUrl(for: languageCode)
        let dataTask: URLSessionDataTask = UrlSessionManager.shared.dataTaskWithETag(with: url) { data, response, error, updateEtag in
            guard let data else {
                completion(.failure(RESTError.noData))
                return
            }
            
            do {
                let labelsDict: [String: String] = try JSONDecoder().decode([String: String].self, from: data)
                updateEtag()
                completion(.success(labelsDict))
                
            } catch {
                completion(.failure(error))
            }
        }
        dataTask.resume()
    }
    
    private static func labelsUrl(for languageCode: String) -> URL {
        URL(string: "\(Constant.InfoCenter.baseUrl)/info-labels-\(languageCode).json")!
    }
}
