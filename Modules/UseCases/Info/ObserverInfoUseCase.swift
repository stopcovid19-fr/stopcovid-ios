// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
//
//  ObserverInfoUseCase.swift
//  TousAntiCovid
//
//  Created by Lunabee Studio / Date - 03/02/2023 - for the TousAntiCovid project.
//

import Protocols

extension UseCases {
    public static func addObserverInfo(_ observer: InfoRepositoryObserver) {
        infoRepository.addObserver(observer)
    }
    
    public static func removeObserverInfo(_ observer: InfoRepositoryObserver) {
        infoRepository.removeObserver(observer)
    }
}
