// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
//
//  MyHealthAppRepository.swift
//  TousAntiCovid
//
//  Created by Lunabee Studio / Date - 27/02/2023 - for the TousAntiCovid project.
//

import Protocols
import AppModel
import Storage
import Server
import ServerSDK

final class MyHealthAppRepositoryImpl: MyHealthRepository {
    typealias AppModel = ParagraphSection

    var objects: [AppModel] = []
    var observers: [Weak<AnyObject>] = .init()

    private let storage: MyHealthStorage = .init()
    private let server: MyHealthServer = .init()

    private var lastUpdatedDate: Date {
        get { storage.lastUpdatedDate }
        set { storage.lastUpdatedDate = newValue }
    }

    init() {
        try? loadLocalObjects()
    }

    func fetchObjects(forceRefresh: Bool, _ completion: @escaping (Error?) -> ()) {
        guard canUpdateData || forceRefresh else {
            completion(nil)
            return
        }
        fetchFile(force: forceRefresh, completion)
    }

    func writeInitialFile() throws {
        guard let url = Bundle.main.url(forResource: Constant.Files.risks.fileName, withExtension: Constant.Files.risks.fileExtension) else {
            throw TACError.fileMissing
        }
        let data: Data = try .init(contentsOf: url)
        try storage.saveLocalData(data, for: nil)
        try loadLocalObjects()
    }

    private var canUpdateData: Bool {
        let lastFetchIsTooOld: Bool = Date().timeIntervalSince1970 - lastUpdatedDate.timeIntervalSince1970 >= ParametersManager.shared.minFilesRefreshInterval
        return lastFetchIsTooOld
    }
}

// MARK: - AppModel -
private extension MyHealthAppRepositoryImpl {
    func toAppModel(_ restModel: RESTParagraphSection) -> AppModel {
        var link: ParagraphLink?
        if let restLink = restModel.link {
            if restLink.type == "ctrl", restLink.action == "GESTURES" {
                link = ParagraphLink(label: restLink.label, action: .gesturesController)
            } else if restLink.type == "web" {
                link = ParagraphLink(label: restLink.label, action: .web(restLink.action))
            }
        }
        return ParagraphSection(title: restModel.section, description: restModel.description, link: link)
    }
}

// MARK: - Observer -
extension MyHealthAppRepositoryImpl {
    func notifyObservers() {
        observers.forEach { weakObserver in
            (weakObserver.value as? MyHealthRepositoryObserver)?.myHealthDidUpdate()
        }
    }
}

// MARK: - All loading methods -
private extension MyHealthAppRepositoryImpl {
    func loadLocalObjects() throws {
        let data: Data = try storage.loadLocalData(for: nil)
        let objects = try JSONDecoder().decode([RESTParagraphSection].self, from: data)
        self.objects = objects.compactMap { toAppModel($0) }
    }
}

// MARK: - All fetching methods -
private extension MyHealthAppRepositoryImpl {
    func fetchFile(force: Bool, _ completion: @escaping (Error?) -> ()) {
        server.fetchData(force: force) { [weak self] result in
            switch result {
            case let .success(objects):
                guard let objects else {
                    // File Not modified
                    self?.lastUpdatedDate = Date()
                    completion(nil)
                    return
                }
                do {
                    let data: Data = try JSONEncoder().encode(objects)
                    self?.objects = objects.compactMap { self?.toAppModel($0) }
                    try self?.storage.saveLocalData(data, for: nil)
                    self?.lastUpdatedDate = Date()
                    self?.notifyObservers()
                    completion(nil)
                } catch {
                    completion(error)
                }
            case let .failure(error):
                completion(error)
            }
        }
    }
}
