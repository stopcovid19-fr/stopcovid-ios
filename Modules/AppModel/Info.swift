// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
//
//  Info.swift
//  TousAntiCovid
//
//  Created by Lunabee Studio / Date - 12/01/2023 - for the TousAntiCovid project.
//

public struct Info {
    public let title: String
    public let description: String
    public let buttonLabel: String?
    public let url: URL?
    public let date: Date
    public let tags: [InfoTag]

    public init(title: String, description: String, buttonLabel: String?, url: URL?, date: Date, tags: [InfoTag]) {
        self.title = title
        self.description = description
        self.buttonLabel = buttonLabel
        self.url = url
        self.date = date
        self.tags = tags
    }
}
