// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
//
//  Double+Extension.swift
//  ExtensionsModule
//
//  Created by Lunabee Studio / Date - 12/01/2023 - for the TousAntiCovid project.
//

public extension Double {
    init?(_ int: Int?) {
        guard let int = int else { return nil }
        self.init(int)
    }

    func formatDuration(style: DateComponentsFormatter.UnitsStyle = .positional, showAllDigits: Bool = true) -> String? {
        let formatter: DateComponentsFormatter = .init()
        if self >= Constant.hourInSeconds {
            formatter.allowedUnits = [.hour, .minute, .second]
        } else {
            formatter.allowedUnits = [.minute, .second]
        }
        formatter.unitsStyle = style
        if showAllDigits {
            formatter.zeroFormattingBehavior = .pad
        }
        return formatter.string(from: self)
    }

    func formatDurationForAccessibility(style: DateComponentsFormatter.UnitsStyle = .positional, showAllDigits: Bool = true) -> String? {
        let date: Date = .init(timeIntervalSince1970: self)
        let calendarComponents: Set<Calendar.Component>
        if self >= Constant.hourInSeconds {
            calendarComponents = [.hour, .minute, .second]
        } else {
            calendarComponents = [.minute, .second]
        }
        let dateComponents: DateComponents = Calendar.current.dateComponents(calendarComponents, from: date)

        return DateComponentsFormatter.localizedString(from: dateComponents, unitsStyle: .spellOut)
    }

}
