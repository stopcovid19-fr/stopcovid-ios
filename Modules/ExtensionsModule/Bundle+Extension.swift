// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
//
//  Bundle+Extension.swift
//  ExtensionsModule
//
//  Created by Lunabee Studio / Date - 12/01/2023 - for the TousAntiCovid project.
//

import UIKit

public extension Bundle {
    var identifier: String { bundleIdentifier.orEmpty }
    var marketingVersion: String { infoDictionary!["CFBundleShortVersionString"] as! String }
    var buildNumber: String { infoDictionary!["CFBundleVersion"] as! String }
    var displayName: String { infoDictionary!["CFBundleDisplayName"] as? String ?? "N/A" }

    var icon: UIImage? {
        guard let icons = infoDictionary?["CFBundleIcons"] as? [String: Any] else { return nil }
        guard let primary = icons["CFBundlePrimaryIcon"] as? [String: Any] else { return nil }
        guard let files = primary["CFBundleIconFiles"] as? [String] else { return nil }
        guard let icon = files.last else { return nil }
        return UIImage(named: icon)
    }
    
    func fileDataFor(fileName: String, ofType: String) -> Data? {
        guard let filePath = path(forResource: fileName, ofType: ofType) else { return nil }
        guard let data = try? Data(contentsOf: URL(fileURLWithPath: filePath)) else { return nil }
        return data
    }
}
