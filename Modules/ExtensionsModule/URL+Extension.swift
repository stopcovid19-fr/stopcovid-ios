// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
//
//  URL+Extension.swift
//  ExtensionsModule
//
//  Created by Lunabee Studio / Date - 12/01/2023 - for the TousAntiCovid project.
//

import Foundation

public extension URL {
    var size: Int64 {
        if isDirectory {
            let urls: [URL] = (try? FileManager.default.contentsOfDirectory(at: self, includingPropertiesForKeys: nil, options: [])) ?? []
            let size: Int64 = urls.reduce(0) { $0 + $1.size }
            return size
        } else {
            return Int64((try? resourceValues(forKeys: [.totalFileSizeKey]))?.totalFileSize ?? 0)
        }
    }

    var isDirectory: Bool { (try? resourceValues(forKeys: [.isDirectoryKey]))?.isDirectory ?? false }
}
