// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
//
//  OptionalData+Extension.swift
//  ExtensionsModule
//
//  Created by Lunabee Studio / Date - 12/01/2023 - for the TousAntiCovid project.
//

import Foundation

public extension Optional where Wrapped == Data {
    var orEmpty: Data {
        switch self {
        case .some(let value):
            return value
        case .none:
            return Data()
        }
    }
}
