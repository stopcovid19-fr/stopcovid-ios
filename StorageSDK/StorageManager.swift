// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
//
//  StorageManager.swift
//  TousAntiCovid
//
//  Created by Lunabee Studio / Date - 27/04/2020 - for the TousAntiCovid project.
//

import UIKit
import KeychainSwift
import RealmSwift

public final class StorageManager {

    enum KeychainKey: String, CaseIterable {
        case dbKey
    }
    
    let keychain: KeychainSwift = KeychainSwift(keyPrefix: "SC")
    public private(set) var wasWiped: Bool = false
    
    private(set) var realm: Realm?
    private var loggingHandler: (_ message: String) -> ()
    
    public init(loggingHandler: @escaping (_ message: String) -> ()) {
        self.loggingHandler = loggingHandler
    }
    
    public func start() {
        loadDb()
    }
    
    public func stop() {
        realm = nil
    }
    
    public func resetWasWiped() {
        wasWiped = false
    }

    public func clearAll(includingDBKey: Bool) {
        clearKeychain(includingDBKey: includingDBKey)
        deleteDb(includingFile: includingDBKey)
        notifyStatusDataChanged()
    }

    public func clearKeychain(includingDBKey: Bool) {
        keychain.allKeys.forEach {
            guard $0.hasPrefix("SC") else { return }
            let keyWithoutPrefix: String = String($0.suffix($0.count - 2))
            if keyWithoutPrefix != KeychainKey.dbKey.rawValue || includingDBKey {
                loggingHandler(formatLog(type: .deletion(.keychain), message: "\(keyWithoutPrefix)"))
                keychain.delete(keyWithoutPrefix)
            }
        }
    }
    
    private func deleteDb(includingFile: Bool) {
        Realm.queue.sync {
            try? realm?.write {
                loggingHandler(formatLog(type: .deletion(.realm), message: "all data"))
                realm?.deleteAll()
            }
            if includingFile {
                realm = nil
                loggingHandler(formatLog(type: .deletion(.realm), message: "database files"))
                Realm.deleteDb()
            }
        }
    }
    
    // MARK: - DB Key -
    private func loadDb() {
        if let key = getDbKey() {
            realm = try! Realm.db(key: key)
        } else if let newKey = Realm.generateEncryptionKey(), !keychain.allKeys.contains("SC\(KeychainKey.dbKey.rawValue)") {
            deleteDb(includingFile: true)
            realm = try! Realm.db(key: newKey)
            save(dbKey: newKey)
        }
    }
    
    private func save(dbKey: Data) {
        keychain.set(dbKey, forKey: KeychainKey.dbKey.rawValue, withAccess: .accessibleAfterFirstUnlockThisDeviceOnly)
    }
    
    private func getDbKey() -> Data? {
        keychain.getData(KeychainKey.dbKey.rawValue)
    }
    
}

public extension StorageManager {
    
    func saveWalletCertificate(_ certificate: RawWalletCertificate) {
        guard let realm = realm else { return }
        Realm.queue.sync {
            let realmRawWalletCertificate: RealmRawWalletCertificate = RealmRawWalletCertificate.from(rawCertificate: certificate)
            try! realm.write {
                realm.add(realmRawWalletCertificate, update: .all)
            }
        }
        notifyWalletCertificateDataChanged()
    }

    func saveWalletCertificates(_ certificates: [RawWalletCertificate]) {
        guard let realm = realm else { return }
        Realm.queue.sync {
            let realmRawWalletCertificates: [RealmRawWalletCertificate] = certificates.map { RealmRawWalletCertificate.from(rawCertificate: $0) }
            try! realm.write {
                realm.add(realmRawWalletCertificates, update: .all)
            }
        }
        notifyWalletCertificateDataChanged()
    }
    
    func walletCertificates() -> [RawWalletCertificate] {
        guard let realm = realm else { return [] }
        var certificates: [RawWalletCertificate] = []
        Realm.queue.sync {
            certificates = realm.objects(RealmRawWalletCertificate.self).map { $0.toRawWalletCertificate() }
        }
        return certificates
    }
    
    func deleteWalletCertificate(id: String) {
        guard let realm = realm else { return }
        Realm.queue.sync {
            if let realmRawWalletCertificate = realm.object(ofType: RealmRawWalletCertificate.self, forPrimaryKey: id) {
                try! realm.write {
                    loggingHandler(formatLog(type: .deletion(.realm), message: "one wallet certificate"))
                    realm.delete(realmRawWalletCertificate)
                }
            }
        }
        notifyWalletCertificateDataChanged()
    }

    func deleteWalletCertificates(ids: [String]) {
        guard let realm = realm else { return }
        Realm.queue.sync {
            let realmRawWalletCertificates: [RealmRawWalletCertificate] = realm.objects(RealmRawWalletCertificate.self).filter { ids.contains($0.id) }
            
            if !realmRawWalletCertificates.isEmpty {
                try! realm.write {
                    loggingHandler(formatLog(type: .deletion(.realm), message: "\(realmRawWalletCertificates.count) wallet certificate(s)"))
                    realm.delete(realmRawWalletCertificates)
                }
            }
        }
        notifyWalletCertificateDataChanged()
    }
    
    func deleteWalletCertificates() {
        guard let realm = realm else { return }
        Realm.queue.sync {
            try! realm.write {
                loggingHandler(formatLog(type: .deletion(.realm), message: "all wallet certificates"))
                realm.delete(realm.objects(RealmRawWalletCertificate.self))
            }
        }
        notifyWalletCertificateDataChanged()
    }
    
}

private extension StorageManager {
    
    func notifyStatusDataChanged() {
        NotificationCenter.default.post(name: .statusDataDidChange, object: nil)
    }
    
    func notifyWalletCertificateDataChanged() {
        NotificationCenter.default.post(name: .walletCertificateDataDidChange, object: nil)
    }
}

// MARK: - Logger -
private extension StorageManager {
    enum LogType {
        case deletion(_ type: DeletionType)
        
        enum DeletionType: String {
            case realm = "Realm"
            case keychain = "Keychain"
        }
        
        var description: String {
            switch self {
            case .deletion(let type):
                return ["Deletion", type.rawValue].joined(separator: "|")
            }
        }
    }
    
    func isWiped() -> Bool {
        // Check if keychain contains db key
        guard !keychain.allKeys.contains("SC\(KeychainKey.dbKey.rawValue)") else { return false }
        // Check if Realm db exist
        let isWiped = realm == nil
        // save that info
        if isWiped {
            wasWiped = true
        }
        return isWiped
    }
    
    func formatIsWiped() -> String? {
        isWiped() ? "⚠️ WIPED" : nil
    }
    
    func formatLog(type: LogType, message: String) -> String {
        [type.description, message, formatIsWiped()].compactMap { $0 }.joined(separator: "|")
    }
}

extension Realm {
    
    static let queue: DispatchQueue = DispatchQueue(label: "RealmQueue", qos: .userInitiated)

    static func db(key: Data?) throws -> Realm {
        guard let key = key else { throw NSError.stLocalizedError(message: "Impossible to decrypt the database", code: 0) }
        return try Self.queue.sync { try Realm(configuration: configuration(key: key), queue: Self.queue) }
    }
    
    static func deleteDb() {
        try? FileManager.default.removeItem(at: dbsDirectoryUrl().appendingPathComponent("db.realm"))
        try? FileManager.default.removeItem(at: dbsDirectoryUrl().appendingPathComponent("db.lock"))
        try? FileManager.default.removeItem(at: dbsDirectoryUrl().appendingPathComponent("db.note"))
        try? FileManager.default.removeItem(at: dbsDirectoryUrl().appendingPathComponent("db.management"))
    }
    
    static func generateEncryptionKey() -> Data? {
        var keyData: Data = Data(count: 64)
        let result: Int32 = keyData.withUnsafeMutableBytes { SecRandomCopyBytes(kSecRandomDefault, 64, $0.baseAddress!) }
        return result == errSecSuccess ? keyData : nil
    }
    
    static private func dbsDirectoryUrl() -> URL {
        var directoryUrl: URL = FileManager.stLibraryDirectory().appendingPathComponent("DBs")
        if !FileManager.default.fileExists(atPath: directoryUrl.path, isDirectory: nil) {
            try? FileManager.default.createDirectory(at: directoryUrl, withIntermediateDirectories: false, attributes: nil)
            try? directoryUrl.stAddSkipBackupAttribute()
        }
        return directoryUrl
    }
    
    static private func configuration(key: Data) -> Realm.Configuration {
        let classes: [Object.Type] = [RealmRawWalletCertificate.self,
                                      RealmBlacklistedIndex.self,
                                      RealmBlacklistedHash.self]
        let migrationBlock: (_ migration: Migration, _ oldSchemaVersion: UInt64) -> Void = { migration, oldSchemaVersion in
            if oldSchemaVersion < 25 {
                migration.deleteData(forType: "RealmBlacklistedDcc")
                migration.deleteData(forType: "RealmBlacklisted2dDoc")
            }
            if oldSchemaVersion < 26 {
                migration.enumerateObjects(ofType: RealmRawWalletCertificate.className()) { oldObject, newObject in
                    if oldObject?["parentId"] != nil, let newObject {
                        migration.delete(newObject)
                    }
                }
            }
        }
        let databaseUrl: URL = dbsDirectoryUrl().appendingPathComponent("db.realm")
        let userConfig: Realm.Configuration = Realm.Configuration(fileURL: databaseUrl, encryptionKey: key, schemaVersion: 26, migrationBlock: migrationBlock, objectTypes: classes)
        return userConfig
    }
    
}
