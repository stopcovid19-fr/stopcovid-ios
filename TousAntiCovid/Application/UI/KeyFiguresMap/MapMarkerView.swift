// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
//
//  MapMarkerView.swift
//  TousAntiCovid
//
//  Created by Lunabee Studio / Date - 02/02/2023 - for the TousAntiCovid project.
//

import UIKit

final class MapMarkerView: UIView, Xibbed {

    @IBOutlet private weak var containerView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    
    var size: CGSize {
        let titleSize: CGSize = titleLabel.text?.size(withAttributes: [.font: Appearance.Cell.Text.titleFont]) ?? .zero
        let subtitleSize: CGSize = subtitleLabel.text?.size(withAttributes: [.font: Appearance.Cell.Text.subtitleFont]) ?? .zero
        return CGSize(width: titleSize.width + subtitleSize.width + 12, height: titleSize.height + subtitleSize.height + 12)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        backgroundColor = .clear
        containerView.backgroundColor = Appearance.Cell.cardBackgroundColor
        containerView.layer.masksToBounds = false
        containerView.layer.cornerRadius = 6
        containerView.layer.shadowOffset = .zero
        containerView.layer.shadowRadius = 3
        containerView.layer.shadowOpacity = 0.2
        containerView.layer.shadowColor = UIColor.black.cgColor
        titleLabel.textAlignment = .center
        titleLabel.font = Appearance.Cell.Text.titleFont
        subtitleLabel.textAlignment = .center
        subtitleLabel.font = Appearance.Cell.Text.subtitleFont
    }
    
    func setup(title: String, subtitle: String?) {
        titleLabel.text = title
        subtitleLabel.isHidden = subtitle == nil
        subtitleLabel.text = subtitle ?? ""
    }
    
}
