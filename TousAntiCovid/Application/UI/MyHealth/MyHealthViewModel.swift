// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
//
//  MyHealthViewModel.swift
//  TousAntiCovid
//
//  Created by Lunabee Studio / Date - 21/02/2023 - for the TousAntiCovid project.
//

import AppModel
import UseCases
import Protocols

struct MyHealthViewModel {
    var sections: [ParagraphSection] { UseCases.myHealthData }

    func fetchData(forceRefresh: Bool) {
        UseCases.fetchMyHealthData(forceRefresh: false)
    }

    func addObservers(_ observer: MyHealthRepositoryObserver) {
        UseCases.addObserverInfo(observer)
    }

    func removeObservers(_ observer: MyHealthRepositoryObserver) {
        UseCases.removeObserverInfo(observer)
    }
}
