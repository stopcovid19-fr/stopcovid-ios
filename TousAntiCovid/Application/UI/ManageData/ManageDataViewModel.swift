// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
//
//  ManageDataViewModel.swift
//  TousAntiCovid
//
//  Created by Lunabee Studio / Date - 20/02/2023 - for the TousAntiCovid project.
//

import UseCases
import AppModel

struct ManageDataViewModel {

    var currentAppLanguage: Language? {
        UseCases.getCurrentAppLanguage(localeLanguageCode: Locale.current.languageCode.orEmpty)
    }

    func setCurrentAppLanguage(to language: Language) {
        let previousLanguage: Language? = currentAppLanguage
        UseCases.setCurrentAppLanguage(to: language)

        if previousLanguage != language {
            LocalizationsManager.shared.reloadLanguage()
            PrivacyManager.shared.reloadLanguage()
            LinksManager.shared.reloadLanguage()
            KeyFiguresExplanationsManager.shared.reloadLanguage()
            FeaturedInfoManager.shared.reloadLanguage()
            UseCases.reloadLanguage()
        }
    }

}
