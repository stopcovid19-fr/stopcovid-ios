// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
//
//  ManageDataController.swift
//  TousAntiCovid
//
//  Created by Lunabee Studio / Date - 05/05/2020 - for the TousAntiCovid project.
//


import UIKit
import PKHUD
import ServerSDK
import AppModel

final class ManageDataController: CVTableViewController {
    
    @UserDefault(key: .autoBrightnessActivated)
    private var autoBrightnessActivated: Bool = true
    private let viewModel: ManageDataViewModel

    init(viewModel: ManageDataViewModel) {
        self.viewModel = viewModel
        super.init(style: .plain)
    }
    
    required init?(coder: NSCoder) {
        fatalError("You must use the standard init() method.")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        updateTitle()
        initUI()
        reloadUI()
        LocalizationsManager.shared.addObserver(self)
    }
    
    deinit {
        LocalizationsManager.shared.removeObserver(self)
    }
    
    func updateTitle() {
        title = "manageDataController.title".localized
    }
    
    override func createSections() -> [CVSection] {
        makeSections {
            CVSection {
                sectionHeaderRow(textPrefix: "manageDataController.userLanguage")
                selectableRow(title: "manageDataController.languageFR".localized,
                              isSelected: viewModel.currentAppLanguage == Language.french, separatorLeftInset: Appearance.Cell.leftMargin,
                              selectionBlock: { [weak self] in
                    self?.viewModel.setCurrentAppLanguage(to: .french)
                })
                selectableRow(title: "manageDataController.languageEN".localized,
                              isSelected: viewModel.currentAppLanguage  == Language.english,
                              selectionBlock: { [weak self] in
                    self?.viewModel.setCurrentAppLanguage(to: .english)
                })
            } header: {
                .groupedHeader
            }
            
            if ParametersManager.shared.smartWalletFeatureActivated {
                CVSection {
                    sectionHeaderRow(textPrefix: "manageDataController.smartWalletActivation")
                    switchRow(textPrefix: "manageDataController.smartWalletActivation",
                              isOn: WalletManager.shared.smartWalletActivated, dynamicSwitchLabel: true) { isOn in
                        WalletManager.shared.smartWalletActivated = isOn
                    }
                } header: {
                    .groupedHeader
                }
            }

            if WalletManager.shared.isWalletActivated {
                CVSection {
                    sectionHeaderRow(textPrefix: "manageDataController.walletData")
                    buttonRow(textPrefix: "manageDataController.walletData", separatorLeftInset: nil) { [weak self] in
                        self?.eraseWalletDataButtonPressed()
                    }
                } header: {
                    .groupedHeader
                }
            }

            CVSection {
                sectionHeaderRow(textPrefix: "common.settings.fullBrightnessSwitch")
                switchRow(textPrefix: "common.settings.fullBrightnessSwitch",
                          isOn: autoBrightnessActivated,
                          dynamicSwitchLabel: true) { [weak self] isOn in
                    self?.autoBrightnessActivated = isOn
                }
            } header: {
                .groupedHeader
            }

            CVSection {
                let logsFilesUrls: [URL] = StackLogger.getLogFilesUrls()
                let subtitle: String =  getLogSubtitle(logsFileUrlsCount: logsFilesUrls.count)
                sectionHeaderRow(title: "manageDataController.logFiles.title".localized, subtitle: subtitle)
                if !logsFilesUrls.isEmpty {
                    buttonRow(textPrefix: "manageDataController.logFiles.share", separatorLeftInset: Appearance.Cell.leftMargin, isDestuctive: false) { [weak self] in
                        guard let self = self else { return }
                        logsFilesUrls.share(from: self)
                    }
                    buttonRow(textPrefix: "manageDataController.logFiles.delete", isDestuctive: true) { [weak self] in
                        self?.deleteLogsFilesButtonPressed()
                    }
                }

            } header: {
                .groupedHeader
            }

            CVSection {
                sectionHeaderRow(textPrefix: "manageDataController.quitStopCovid")
                buttonRow(textPrefix: "manageDataController.quitStopCovid", isDestuctive: true) { [weak self] in
                    self?.quitButtonPressed()
                }
            } header: {
                .groupedHeader
            }
        }
    }

    private func updateLeftBarButton() {
        navigationItem.leftBarButtonItem?.title = "common.close".localized
        navigationItem.leftBarButtonItem?.accessibilityHint = "accessibility.closeModal.zGesture".localized
    }
    
    private func initUI() {
        tableView.backgroundColor = Appearance.Controller.cardTableViewBackgroundColor
        tableView.showsVerticalScrollIndicator = false
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "common.close".localized, style: .plain, target: self, action: #selector(didTouchCloseButton))
        navigationItem.leftBarButtonItem?.accessibilityHint = "accessibility.closeModal.zGesture".localized
    }
    
    @objc private func didTouchCloseButton() {
        dismiss(animated: true, completion: nil)
    }

    private func sectionHeaderRow(textPrefix: String) -> CVRow {
        sectionHeaderRow(title: "\(textPrefix).title".localized,
                         subtitle: "\(textPrefix).subtitle".localized)
    }
    
    private func sectionHeaderRow(title: String, subtitle: String) -> CVRow {
        var textRow: CVRow = CVRow(title: title,
                                   subtitle: subtitle,
                                   xibName: .textCell,
                                   theme: CVRow.Theme(topInset: Appearance.Cell.Inset.normal,
                                                      bottomInset: Appearance.Cell.Inset.normal,
                                                      textAlignment: .natural,
                                                      titleFont: { Appearance.Cell.Text.smallHeadTitleFont },
                                                      separatorLeftInset: Appearance.Cell.leftMargin))
        textRow.theme.backgroundColor = Appearance.Cell.cardBackgroundColor
        return textRow
    }

    private func selectableRow(title: String, isSelected: Bool, separatorLeftInset: CGFloat? = nil, selectionBlock: @escaping () -> ()) -> CVRow {
        CVRow(title: title,
              isOn: isSelected,
              xibName: .selectableCell,
              theme: CVRow.Theme(backgroundColor: Appearance.Cell.cardBackgroundColor,
                                 topInset: Appearance.Cell.Inset.normal,
                                 bottomInset: Appearance.Cell.Inset.normal,
                                 textAlignment: .natural,
                                 titleFont: { Appearance.Cell.Text.standardFont },
                                 separatorLeftInset: separatorLeftInset),
              selectionAction: { _ in
            selectionBlock()
        })
    }
    
    private func switchRow(textPrefix: String, separatorLeftInset: CGFloat? = nil, isOn: Bool, dynamicSwitchLabel: Bool, handler: @escaping (_ isOn: Bool) -> ()) -> CVRow {
        CVRow(title: (dynamicSwitchLabel ? (isOn ? "\(textPrefix).switch.on" : "\(textPrefix).switch.off") : "\(textPrefix).button").localized,
              isOn: isOn,
              xibName: .standardSwitchCell,
              theme: CVRow.Theme(backgroundColor: Appearance.Cell.cardBackgroundColor,
                                 topInset: Appearance.Cell.Inset.small,
                                 bottomInset: Appearance.Cell.Inset.small,
                                 textAlignment: .left,
                                 titleFont: { Appearance.Cell.Text.standardFont },
                                 separatorLeftInset: separatorLeftInset),
              valueChanged: { [weak self] value in
            guard let isOn = value as? Bool else { return }
            handler(isOn)
            self?.reloadUI()
        })
    }
    
    private func buttonRow(textPrefix: String, separatorLeftInset: CGFloat? = nil, isDestuctive: Bool = false, handler: @escaping () -> ()) -> CVRow {
        var buttonRow: CVRow = CVRow(title: "\(textPrefix).button".localized,
                                     xibName: .standardCell,
                                     theme: CVRow.Theme(topInset: Appearance.Cell.Inset.normal,
                                                        bottomInset: Appearance.Cell.Inset.normal,
                                                        textAlignment: .natural,
                                                        titleFont: { Appearance.Cell.Text.standardFont },
                                                        titleColor: isDestuctive ? Asset.Colors.error.color : Asset.Colors.tint.color,
                                                        separatorLeftInset: separatorLeftInset,
                                                        accessoryType: UITableViewCell.AccessoryType.none),
                                     selectionAction: { _ in handler() },
                                     willDisplay: { view in
            guard let cell = view as? CVTableViewCell else { return }
            cell.cvTitleLabel?.accessibilityTraits = .button
        })
        buttonRow.theme.backgroundColor = Appearance.Cell.cardBackgroundColor
        return buttonRow
    }
    
    @objc private func didTouchBackButton() {
        navigationController?.popViewController(animated: true)
    }
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        navigationChildController?.scrollViewDidScroll(scrollView)
    }
    
    private func eraseWalletDataButtonPressed() {
        showAlert(title: "manageDataController.walletData.confirmationDialog.title".localized,
                  message: "manageDataController.walletData.confirmationDialog.message".localized,
                  okTitle: "common.yes".localized,
                  isOkDestructive: true,
                  cancelTitle: "common.no".localized, handler: { [weak self] in
            WalletManager.shared.clearAllData()
            self?.showFlash()
        })
    }
    
    private func quitButtonPressed() {
        showAlert(title: "manageDataController.quitStopCovid.confirmationDialog.title".localized,
                  message: "manageDataController.quitStopCovid.confirmationDialog.message".localized,
                  okTitle: "common.yes".localized,
                  isOkDestructive: true,
                  cancelTitle: "common.no".localized, handler: { [weak self] in 
            self?.processQuitActions()
        })
    }

    private func getLogSubtitle(logsFileUrlsCount: Int) -> String {
        var subtitle: String =  "manageDataController.logFiles.subtitle".localized
        if logsFileUrlsCount > 0 {
            subtitle += "\n\n\(String(format: "manageDataController.logFiles.logsFilesCount".localized, logsFileUrlsCount))"
        } else {
            subtitle += "\n\n\("manageDataController.logFiles.noLogs".localized)"
        }
        return subtitle
    }

    private func deleteLogsFilesButtonPressed() {
        showAlert(title: "manageDataController.logFiles.delete.confirmationDialog.title".localized,
                  message: "manageDataController.logFiles.delete.confirmationDialog.message".localized,
                  okTitle: "common.yes".localized,
                  isOkDestructive: true,
                  cancelTitle: "common.no".localized, handler:  { [weak self] in
            StackLogger.deleteAllLogsFiles()
            HUD.flash(.success)
            self?.reloadUI()
        })
    }
    
    private func processQuitActions() {
        KeyFiguresManager.shared.currentPostalCode = nil
        WalletManager.shared.clearAllData()
        ETagManager.shared.clearAllData()
        SVETagManager.shared.clearAllData()

        NotificationsManager.shared.removeAllPendingNotifications()
        NotificationCenter.default.post(name: .changeAppState, object: RootCoordinator.State.onboarding, userInfo: nil)
    }
    
}

extension ManageDataController: LocalizationsChangesObserver {
    
    func localizationsChanged() {
        updateTitle()
        updateLeftBarButton()
        reloadUI()
    }
    
}
