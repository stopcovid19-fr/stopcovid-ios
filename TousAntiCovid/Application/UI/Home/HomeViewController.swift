// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
//
//  HomeViewController.swift
//  TousAntiCovid
//
//  Created by Lunabee Studio / Date - 09/04/2020 - for the TousAntiCovid project.
//

import UIKit

final class HomeViewController: CVHomeTableViewController, HomeBased {
    let viewModel: HomeBasedViewModel
    var outputs: HomeOutputs
    var popRecognizer: InteractivePopGestureRecognizer?
    var wasActivated: Bool = false
    var isChangingState: Bool = false
    var didShowRobertErrorAlertOnce: Bool = false
    var areNotificationsAuthorized: Bool?
    var isWaitingForNeededInfo: Bool = true

    @UserDefault(key: .latestAvailableBuild)
    var latestAvailableBuild: Int?
        
    @UserDefault(key: .didAlreadyShowUserLanguage)
    var didAlreadyShowUserLanguage: Bool = false
   
    var reloadingTimer: Timer?
    private var shouldNextReloadBeAnimated: Bool = false

    init(viewModel: HomeBasedViewModel, outputs: HomeOutputs) {
        self.viewModel = viewModel
        self.outputs = outputs
        super.init(style: .plain)
    }
    
    required init?(coder: NSCoder) {
        fatalError("Must use the other init method")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initUI(scrollView: tableView)
        onViewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        onViewDidAppear()
    }
    
    deinit {
        removeObservers()
        outputs.deinitBlock()
    }
    
    override func createSections() -> [CVSection] {
        buildModel().compactMap { $0.mapToTableSection() }
    }
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        navigationChildController?.scrollViewDidScroll(scrollView)
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        if #available(iOS 12.0, *) {
            if previousTraitCollection?.userInterfaceStyle != traitCollection.userInterfaceStyle { startReloadingTimer(shouldNextReloadBeAnimated: false) }
        }
    }
}

// MARK: - Reloading timer -
extension HomeViewController {
    func createReloadingTimer(shouldNextReloadBeAnimated: Bool) -> Timer {
        self.shouldNextReloadBeAnimated = shouldNextReloadBeAnimated
        return .init(timeInterval: HomeConstant.reloadingTimeInterval, target: self, selector: #selector(triggerReloadingTimer), userInfo: nil, repeats: false)
    }
    
    @objc func triggerReloadingTimer() {
        reloadUI(animated: shouldNextReloadBeAnimated)
    }
}
