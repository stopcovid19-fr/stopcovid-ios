// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
//
//  HomeBasedViewModel.swift
//  TousAntiCovid
//
//  Created by Lunabee Studio / Date - 07/02/2023 - for the TousAntiCovid project.
//

import Protocols
import UseCases
import AppModel

final class HomeBasedViewModel {

    var isCurrentLanguageSupported: Bool {
        UseCases.isCurrentLanguageSupported(localeLanguageCode: Locale.current.languageCode.orEmpty)
    }

    var infos: [Info] { UseCases.infos }

    func addObservers(_ observer: InfoRepositoryObserver) {
        UseCases.addObserverInfo(observer)
    }
    
    func removeObservers(_ observer: InfoRepositoryObserver) {
        UseCases.removeObserverInfo(observer)
    }

}
