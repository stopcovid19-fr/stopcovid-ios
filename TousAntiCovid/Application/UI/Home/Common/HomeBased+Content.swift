// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
//
//  HomeBased+Content.swift
//  TousAntiCovid
//
//  Created by Lunabee Studio / Date - 14/03/2022 - for the TousAntiCovid project.
//

import UIKit
import ServerSDK
import PKHUD
import Charts
import AppModel
import UseCases

extension HomeBased {
    func firstGroup() -> CVGroup {
        CVGroup.init(id: "firstGroup", rows: [titleRow(),
                                              homeNotificationRow(),
                                              imageHeaderRow(),
                                              qrScanRow(),
                                              appUpdateRow()].compactMap { $0 })
    }
    
    func walletGroup() -> CVGroup {
        .init(id: "walletGroup",
              title: "home.walletSection.title".localized,
              rows: [favoriteCertificateRow(), walletRow()].compactMap { $0 })
    }

    func featuredInfoGroup() -> CVGroup? {
        guard ParametersManager.shared.displayInTheSpotlight else { return nil }
        guard let featuredInfoRows = featuredInfoRows() else { return nil }
        var section: CVGroup = .init(id: "featuredGroup",
                                     title: "home.featuredInfoSection.title".localized,
                                     orientation: featuredInfoRows.count > 1 ? .horizontal(.fixed(0.7)) : .vertical(NSDirectionalEdgeInsets(top: .zero, leading: Appearance.Cell.leftMargin, bottom: .zero, trailing: Appearance.Cell.rightMargin), .zero),
                                     rows: featuredInfoRows,
                                     primaryActionTitle: "")
        if #available(iOS 13.0, *) {} else {
            section.primaryActionTitle = "home.featuredInfoSection.all".localized
            section.primaryAction = { [weak self] in
                self?.outputs.didTouchSeeAllFeaturedInfos()
            }
        }
        return section
    }

    func vaccinationGroup() -> CVGroup? {
        guard ParametersManager.shared.displayHomeVaccination else { return nil }
        return CVGroup(id: "vaccinationGroup",
              title: "home.vaccinationSection.title".localized,
              rows: [urgentDGSRow(), vaccinationRow()].compactMap { $0 })
    }
    
    func contactGroup() -> CVGroup? {
        guard ParametersManager.shared.displayHomeRecommendations else { return nil }
        return .init(id: "contactGroup",
              title: "home.contactSection.title".localized,
              rows: [contactTipsRow(), deactivatedTracingDeclareRow()].compactMap { $0 })
    }
    
    func keyFiguresGroups() -> [CVGroup]? {
        guard ParametersManager.shared.displayHomeKeyFigures else { return nil }

        var sections: [CVGroup] = []
        guard !KeyFiguresManager.shared.keyFigures.isEmpty else {
            return [CVGroup(id: "keyFiguresWarningGroup",
                            title: "home.infoSection.keyFigures".localized,
                            rows: [keyFigureWarningRow(), keyFigureRetryRow()])]
        }
        
        if let warningRow = keyFiguresWarningRow() {
            sections.append(.init(id: "keyFiguresWarningGroup",
                                  title: nil,
                                  rows: [warningRow],
                                  primaryActionTitle: nil))
        }
        
        if let keyFiguresRows = keyFiguresRows() {
            sections.append(.init(id: "keyFiguresDataGroup",
                                  title: nil,
                                  orientation: keyFiguresRows.count > 1 ? .horizontal(.adaptive) : .vertical(NSDirectionalEdgeInsets(top: .zero, leading: Appearance.Cell.leftMargin, bottom: .zero, trailing: Appearance.Cell.rightMargin), .zero),
                                  rows: keyFiguresRows,
                                  primaryActionTitle: nil))
        }

        var keyFiguresOptionsRows: [CVRow] = [noFavoriteKeyFigureRow(), newPostalCodeRow(), addPostalCodeActionRow(), updatePostalCodeRow(), comparisonChartRow()].compactMap { $0 }
        if #available(iOS 13.0, *) {
            if let keyFigureMapRow = keyFigureMapRow() {
                keyFiguresOptionsRows.append(keyFigureMapRow)
            }
        }
        sections.append(.init(id: "keyFiguresOptionsGroup",
                              title: nil,
                              rows: keyFiguresOptionsRows,
                              primaryActionTitle: nil))

        if !sections.isEmpty {
            sections[0].title = "home.infoSection.keyFigures".localized
            sections[0].primaryActionTitle = "home.figuresSection.all".localized
            sections[0].primaryAction = { [weak self] in
                self?.outputs.didTouchKeyFigures()
            }
        }
        
        return sections
    }

    func infoGroups() -> [CVGroup] {
        let infoRows: [CVRow]? = infoRows()
        var sections: [CVGroup] = []

        if let infoRows {
            sections.append(.init(id: "infoGroup",
                                  orientation: infoRows.count > 1 ? .horizontal(.adaptive) : .vertical(NSDirectionalEdgeInsets(top: .zero, leading: Appearance.Cell.leftMargin, bottom: .zero, trailing: Appearance.Cell.rightMargin), .zero),
                                  rows: infoRows,
                                  primaryActionTitle: "home.infoSection.all".localized) { [weak self] in
                self?.outputs.didTouchInfo(nil)
            })
        }

        if let usefulLinksRow = usefulLinksRow(inseted: infoRows != nil) {
            sections.append(.init(id: "usefullLinksGroup", rows: [usefulLinksRow]))
        }

        if !sections.isEmpty {
            sections[0].title = "home.infoSection.news".localized
        }
        return sections
    }
    
    func moreGroup() -> CVGroup {
        .init(id: "moreGroup",
              title: "home.moreSection.title".localized,
              rows: moreRows())
    }
    
}

// MARK: - First Section -
private extension HomeBased {
    func titleRow() -> CVRow {
        .init(title: title,
              xibName: .headerCell,
              theme: CVRow.Theme(topInset: Appearance.Cell.Inset.small,
                                 bottomInset: Appearance.Cell.Inset.small,
                                 textAlignment: .natural,
                                 titleFont: { Appearance.Controller.titleFont },
                                 titleLinesCount: 1,
                                 separatorLeftInset: nil),
              willDisplay: { [weak self] view in
            guard let cell = view as? CVCell else { return }
            self?.navigationChildController?.updateLabel(titleLabel: cell.cvTitleLabel, containerView: cell)
            cell.cvTitleLabel?.alpha = self?.navigationChildController?.isLargeTitleVisible == true ? 1.0 : 0.0
            cell.isAccessibilityElement = false
            cell.accessibilityElements = []
            cell.accessibilityElementsHidden = true
        })
    }
    
    func imageHeaderRow() -> CVRow {
        CVRow(image: Asset.Images.noTracingRegistered.image,
              xibName: .stateImageCell,
              theme: .init(imageRatio: Appearance.Cell.Image.homeDeactivatedTracingStateRatio))
    }
    
    func homeNotificationRow() -> CVRow? {
        guard let notif = ParametersManager.shared.homeNotification else { return nil }
        guard notif.hasContent && !HomeNotificationManager.shared.wasAlreadyClosed(notification: notif) else { return nil }
        return CVRow(
            title: notif.title,
            subtitle: notif.subtitle,
            image: Asset.Images.homeNotifCard.image,
            xibName: .homeNotificationCell,
            theme: CVRow.Theme(backgroundColor: Appearance.Cell.cardBackgroundColor,
                               topInset: Appearance.Cell.leftMargin / 2,
                               bottomInset: Appearance.Cell.leftMargin / 2,
                               textAlignment: .natural),
            selectionAction: { _ in
                notif.url?.openInSafari()
            }, secondarySelectionAction: { [weak self] in
                UIImpactFeedbackGenerator(style: .light).impactOccurred()
                HomeNotificationManager.shared.close(notification: notif)
                self?.reloadUI(animated: true, animatedView: nil, completion: nil)
            })
    }
    
    func qrScanRow() -> CVRow {
        CVRow(title: "home.qrScan.button.title".localized,
              xibName: .classicButtonForHomeCell,
              theme: CVRow.Theme(topInset: .zero, bottomInset: .zero, buttonStyle: .secondary),
              selectionAction: { [weak self] _ in
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                self?.openQrScan()
            }
        })
    }
    
    func appUpdateRow() -> CVRow? {
        guard let latestAvailableBuild = latestAvailableBuild, latestAvailableBuild > Int(UIApplication.shared.buildNumber) ?? 0 else { return nil }
        return CVRow(title: "home.appUpdate.cell.title".localized,
                     subtitle: "home.appUpdate.cell.subtitle".localized,
                     image: Asset.Images.updateApp.image,
                     xibName: .updateAppCell,
                     theme: CVRow.Theme(backgroundColor: Appearance.Cell.cardBackgroundColor,
                                        topInset: Appearance.Cell.Inset.normal,
                                        bottomInset: .zero,
                                        textAlignment: .natural),
                     selectionAction: { [weak self] _ in
            self?.outputs.didTouchAppUpdate()
        })
    }
}

// MARK: - Wallet section
private extension HomeBased {
    func favoriteCertificateRow() -> CVRow? {
        guard let certificate = WalletManager.shared.favoriteCertificate else { return nil }
        return CVRow(title: "home.walletSection.favoriteCertificate.cell.title".localized,
                     subtitle: "home.walletSection.favoriteCertificate.cell.subtitle".localized,
                     image: certificate.value.qrCode(small: true),
                     xibName: .favoriteCertificateCell,
                     theme: CVRow.Theme(backgroundColor: Appearance.Cell.cardBackgroundColor,
                                        topInset: .zero,
                                        bottomInset: Appearance.Cell.Inset.normal,
                                        textAlignment: .natural),
                     selectionAction: { [weak self] _ in
            self?.outputs.didTouchCertificate(certificate)
        })
    }

    func walletRow() -> CVRow {
        let backgroundColor: UIColor
        let image: UIImage
        let subtitle: String
        let textColor: UIColor
        if WalletManager.shared.shouldUseSmartWallet {
            switch WalletManager.shared.walletSmartState {
            case .normal:
                backgroundColor = Appearance.tintColor
                image = Asset.Images.walletCard.image
                subtitle = "home.attestationSection.sanitaryCertificates.cell.subtitle".localized
                textColor = Appearance.Button.Primary.titleColor
            case .eligibleSoon:
                backgroundColor = Asset.Colors.smartWalletInfo.color
                image = Asset.Images.eligible.image
                subtitle = "home.attestationSection.sanitaryCertificates.eligibleSoon.cell.subtitle".localized
                textColor = .white
            case .eligible:
                backgroundColor = Asset.Colors.smartWalletInfo.color
                image = Asset.Images.eligible.image
                subtitle = "home.attestationSection.sanitaryCertificates.eligible.cell.subtitle".localized
                textColor = .white
            case .expiredSoon:
                backgroundColor = Asset.Colors.bottomWarning.color
                image = Asset.Images.expiredSoon.image
                subtitle = "home.attestationSection.sanitaryCertificates.expiredSoon.cell.subtitle".localized
                textColor = .black
            case .expired:
                backgroundColor = Asset.Colors.error.color
                image = Asset.Images.expired.image
                subtitle = "home.attestationSection.sanitaryCertificates.expired.cell.subtitle".localized
                textColor = .white
            }
        } else {
            backgroundColor = Appearance.tintColor
            image = Asset.Images.walletCard.image
            subtitle = "home.attestationSection.sanitaryCertificates.cell.subtitle".localized
            textColor = Appearance.Button.Primary.titleColor
        }
        return CVRow(title: "home.attestationSection.sanitaryCertificates.cell.title".localized,
                     subtitle: subtitle,
                     image: image,
                     xibName: .sanitaryCertificatesWalletCell,
                     theme: CVRow.Theme(backgroundColor: backgroundColor,
                                        topInset: .zero,
                                        bottomInset: .zero,
                                        textAlignment: .natural,
                                        titleColor: textColor,
                                        subtitleColor: textColor),
                     selectionAction: { [weak self] _ in
            self?.outputs.didTouchSanitaryCertificates(nil)
        })
    }
}

// MARK: - Vaccination section
private extension HomeBased {
    func urgentDGSRow() -> CVRow? {
        guard ParametersManager.shared.shouldDisplayUrgentDgs else { return nil }
        return CVRow(title: "home.healthSection.dgsUrgent.title".localized,
                     subtitle: "home.healthSection.dgsUrgent.subtitle".localized,
                     image: Asset.Images.dgsurgent.image,
                     xibName: .urgentDgsCell,
                     theme: CVRow.Theme(backgroundColor: Appearance.Cell.cardBackgroundColor,
                                        topInset: .zero,
                                        bottomInset: Appearance.Cell.Inset.normal,
                                        textAlignment: .natural,
                                        titleColor: Appearance.Cell.Text.titleColor,
                                        subtitleColor: Appearance.Cell.Text.titleColor),
                     selectionAction: { [weak self] _ in
            self?.outputs.didTouchUrgentDgs()
        })
    }
    
    func vaccinationRow() -> CVRow? {
        guard ParametersManager.shared.displayVaccination else { return nil }
        return CVRow(title: "home.vaccinationSection.cellTitle".localized,
                     subtitle: "home.vaccinationSection.cellSubtitle".localized,
                     image: Asset.Images.centresvaxx.image,
                     xibName: .vaccinationCell,
                     theme: CVRow.Theme(backgroundColor: Appearance.Cell.cardBackgroundColor,
                                        topInset: .zero,
                                        bottomInset: .zero,
                                        textAlignment: .natural,
                                        titleColor: Asset.Colors.gradientEndGreen.color),
                     selectionAction: { [weak self] _ in
            self?.outputs.didTouchVaccination()
        })
    }
}

// MARK: - Contact
private extension HomeBased {
    func contactTipsRow() -> CVRow? {
        CVRow(title: "home.healthSection.contactTips.title".localized,
              subtitle: "home.healthSection.contactTips.subtitle".localized,
              image: Asset.Images.healthCard.image,
              xibName: .declareCell,
              theme: CVRow.Theme(backgroundColor: Appearance.tintColor,
                                 topInset: .zero,
                                 bottomInset: Appearance.Cell.Inset.normal,
                                 textAlignment: .natural,
                                 titleColor: Appearance.Button.Primary.titleColor,
                                 subtitleColor: Appearance.Button.Primary.titleColor,
                                 maskedCorners: .all),
              selectionAction: { [weak self] _ in
            self?.outputs.didTouchHealth()
        })
    }

    func deactivatedTracingDeclareRow() -> CVRow {
        return CVRow(title: "home.declareSection.noTracing.cellTitle".localized,
                     subtitle: "home.declareSection.noTracing.cellSubtitle".localized,
                     image: Asset.Images.declareCard.image,
                     xibName: .declareCell,
                     theme: CVRow.Theme(backgroundColor: Appearance.Button.Disabled.backgroundColor,
                                        topInset: .zero,
                                        bottomInset: .zero,
                                        textAlignment: .natural,
                                        titleColor: Appearance.Button.Disabled.titleColor,
                                        subtitleColor: Appearance.Button.Disabled.titleColor),
                     selectionAction: { [weak self] _ in
            self?.outputs.showNoTracingExplanationsBottomSheet()
        })
    }
}

// MARK: - Key Figures Section
private extension HomeBased {
    func keyFiguresRows() -> [CVRow]? {
        guard !KeyFiguresManager.shared.favoriteKeyFigures.isEmpty else { return nil }
        if #available(iOS 13.0, *) {
            let keyFiguresCells: [CVRow] = KeyFiguresManager.shared.favoriteKeyFigures.compactMap { keyFigureRow($0) }
            let seeAllCell: CVRow = .init(title: "home.figuresSection.all".localized,
                                          subtitle: "keyFiguresSection", // Use to identify the cell as unique
                                          xibName: .seeAllCardCell,
                                          theme: .init(backgroundColor: Appearance.Button.Secondary.backgroundColor,
                                                       topInset: .zero,
                                                       bottomInset: .zero,
                                                       leftInset: .zero,
                                                       rightInset: .zero,
                                                       textAlignment: .center,
                                                       titleFont: { Appearance.Cell.Text.titleFont },
                                                       titleColor: Appearance.Button.Secondary.titleColor,
                                                       titleLinesCount: 1),
                                          selectionAction: { [weak self] _ in
                self?.outputs.didTouchKeyFigures()
            })
            return keyFiguresCells + [seeAllCell]
        } else {
            return [keyFigureRow(KeyFiguresManager.shared.favoriteKeyFigures[0], inseted: true)]
        }
    }
    
    func keyFiguresWarningRow() -> CVRow? {
        guard !"home.infoSection.warning".localizedOrEmpty.isEmpty else { return nil }
        return CVRow(title: "home.infoSection.warning".localized,
                     xibName: .homeStandardCardCell,
                     theme: .init(backgroundColor: Appearance.Cell.cardBackgroundColor,
                                  topInset: .zero,
                                  bottomInset: Appearance.Cell.Inset.normal,
                                  textAlignment: .natural,
                                  titleFont: { Appearance.Cell.Text.subtitleFont },
                                  titleLinesCount: 3),
                     selectionAction: { [weak self] cell in
            self?.outputs.didTouchWarningKeyFigures()
        })
    }
    
    func keyFigureRow(_ keyFigure: KeyFigure, inseted: Bool = false) -> CVRow {
        var place: String? = nil
        if let departmentKeyFigure = keyFigure.currentDepartmentSpecificKeyFigure {
            place = departmentKeyFigure.label
        } else if keyFigure.currentDepartmentSpecificKeyFigure == nil, KeyFiguresManager.shared.currentPostalCode != nil {
            place = "common.country.france".localized.capitalized
        }
        return CVRow(title: (keyFigure.currentDepartmentSpecificKeyFigure?.valueToDisplay ?? keyFigure.valueGlobalToDisplay).formattingValueWithThousandsSeparatorIfPossible(),
                     subtitle: keyFigure.shortLabel,
                     accessoryText: place,
                     xibName: .keyFigureCollectionViewCell,
                     theme: .init(backgroundColor: Appearance.Cell.cardBackgroundColor,
                                  topInset: .zero,
                                  bottomInset: .zero,
                                  leftInset: inseted ? Appearance.Cell.Inset.normal : .zero,
                                  rightInset: inseted ? Appearance.Cell.Inset.normal : .zero,
                                  textAlignment: .center,
                                  titleFont: { Appearance.Cell.Text.headTitleFont3 },
                                  titleColor: .white,
                                  titleLinesCount: 1,
                                  subtitleColor: .white),
                     associatedValue: keyFigure,
                     selectionAction: { [weak self] _ in
            self?.outputs.didTouchKeyFigure(keyFigure)
        })
    }
    
    func noFavoriteKeyFigureRow() -> CVRow? {
        guard KeyFiguresManager.shared.favoriteKeyFigures.isEmpty else { return nil }
        return CVRow(title: "keyfigures.noFavorite.cell.title".localized,
                     subtitle: "keyfigures.noFavorite.cell.add.subtitle".localized,
                     xibName: .homeStandardCardCell,
                     theme: CVRow.Theme(backgroundColor: Appearance.Cell.cardBackgroundColor,
                                        topInset: .zero,
                                        bottomInset: .zero,
                                        textAlignment: .natural),
                     selectionAction: { [weak self] _ in
            self?.outputs.didTouchFavoriteKeyFigures()
        })
    }

    func updatePostalCodeRow() -> CVRow? {
        guard KeyFiguresManager.shared.displayDepartmentLevel else { return nil }
        guard let currentPostalCode = KeyFiguresManager.shared.currentPostalCode else { return nil }
        let title: String = String(format: "common.updatePostalCode".localized, currentPostalCode)
        return CVRow(title: title,
                     subtitle: "common.updatePostalCode.end".localized,
                     image: Asset.Images.location.image,
                     xibName: .homeStandardCardHorizontalCell,
                     theme:  CVRow.Theme(backgroundColor: Appearance.Cell.cardBackgroundColor,
                                         topInset: Appearance.Cell.Inset.normal,
                                         bottomInset: .zero,
                                         textAlignment: .natural,
                                         subtitleFont: { Appearance.Cell.Text.standardFont },
                                         subtitleColor: Appearance.Cell.Text.headerTitleColor,
                                         imageTintColor: Appearance.Cell.Text.headerTitleColor,
                                         imageSize: CGSize(width: 24.0, height: 24.0)),
                     selectionAction: { [weak self] _ in
            self?.didTouchUpdateLocation()
        }, willDisplay: { [weak self] cell in
            guard let self = self else { return }
            cell.accessibilityLabel = title
            cell.accessibilityTraits = []
            cell.accessibilityCustomActions = [
                UIAccessibilityCustomAction(name: "common.updatePostalCode.end".localized, target: self, selector: #selector(self.accessibilityDidActivateChangeLocation)),
                UIAccessibilityCustomAction(name: "common.delete".localized, target: self, selector: #selector(self.accessibilityDidActivateDeleteLocation))
            ]
        })
    }

    func newPostalCodeRow() -> CVRow? {
        guard !KeyFiguresManager.shared.featuredKeyFigures.isEmpty && KeyFiguresManager.shared.canShowCurrentlyNeededFile else { return nil }
        guard KeyFiguresManager.shared.displayDepartmentLevel else { return nil }
        guard KeyFiguresManager.shared.currentPostalCode == nil else { return nil }
        return CVRow(title: "home.infoSection.newPostalCode".localized,
                     subtitle: "home.infoSection.newPostalCode.subtitle".localized,
                     image: Asset.Images.location.image,
                     xibName: .standardCardWithImageCell,
                     theme:  CVRow.Theme(backgroundColor: Appearance.tintColor,
                                         topInset: Appearance.Cell.Inset.normal,
                                         bottomInset: .zero,
                                         textAlignment: .natural,
                                         titleColor: Appearance.Button.Primary.titleColor,
                                         subtitleColor: Appearance.Button.Primary.titleColor,
                                         imageTintColor: Appearance.Button.Primary.titleColor,
                                         separatorLeftInset: Appearance.Cell.leftMargin,
                                         separatorRightInset: Appearance.Cell.leftMargin,
                                         maskedCorners: UIAccessibility.isVoiceOverRunning ? .all : .top),
                     selectionAction: { [weak self] _ in
            self?.didTouchUpdateLocation()
        })
    }
    
    func addPostalCodeActionRow() -> CVRow? {
        guard !KeyFiguresManager.shared.featuredKeyFigures.isEmpty && KeyFiguresManager.shared.canShowCurrentlyNeededFile else { return nil }
        guard KeyFiguresManager.shared.displayDepartmentLevel else { return nil }
        guard KeyFiguresManager.shared.currentPostalCode == nil else { return nil }
        guard !UIAccessibility.isVoiceOverRunning else { return nil }
        return CVRow(title: "home.infoSection.newPostalCode.button".localized,
                     xibName: .homeStandardCardCell,
                     theme:  CVRow.Theme(backgroundColor: Appearance.Cell.Isolation.actionBackgroundColor,
                                         topInset: .zero,
                                         bottomInset: .zero,
                                         textAlignment: .natural,
                                         titleFont: { Appearance.Cell.Text.actionTitleFont },
                                         titleColor: Appearance.Button.Primary.titleColor,
                                         separatorLeftInset: nil,
                                         separatorRightInset: nil,
                                         maskedCorners: .bottom),
                     selectionAction: { [weak self] _ in
            self?.didTouchUpdateLocation()
        })
    }
    
    func comparisonChartRow() -> CVRow? {
        guard let chartData = KeyFiguresManager.shared.generateComparisonChartData(
            keyFigures: KeyFiguresManager.shared.comparedKeyFigures,
            daysCount: ChartRange.year.rawValue,
            withFooter: "home.figuresSection.keyFigures.chart.footer".localized) else { return nil }
        let areComparable: Bool = KeyFiguresManager.shared.comparedKeyFigures.haveSameMagnitude
        let chartView: ChartViewBase? = ChartViewBase.create(chartData1: chartData.chartData1,
                                                             chartData2: chartData.chartData2,
                                                             sameOrdinate: areComparable,
                                                             allowInteractions: false)
        return CVRow(xibName: self is UITableViewController ? .homeKeyFigureChartTableCell : .homeKeyFigureChartCollectionCell,
                     theme: CVRow.Theme(backgroundColor: Appearance.Cell.cardBackgroundColor,
                                        topInset: Appearance.Cell.Inset.normal,
                                        bottomInset: .zero,
                                        textAlignment: .natural),
                     associatedValue: ComparisonChartAssociatedValue(datas: [chartData.chartData1, chartData.chartData2], chartViewBase: chartView),
                     selectionActionWithCell: { [weak self] cell in
            self?.outputs.didTouchComparisonChartSharing((cell as? HomeKeyFigureChartCellable)?.captureWithoutFooter())
        },
                     selectionAction: { [weak self] _ in
            self?.outputs.didTouchComparisonChart()
        })
    }
    
    @available(iOS 13.0, *)
    func keyFigureMapRow() -> CVRow? {
        if KeyFiguresMapManager.shared.keyFiguresMapActivated {
            return CVRow(title: "home.infoSection.keyFigures.map".localized,
                         subtitle: "home.infoSection.keyfigures.map.subtitle".localized,
                         image: Asset.Images.franceMap.image,
                         xibName: .keyFiguresMapCell,
                         theme: CVRow.Theme(backgroundColor: Appearance.Cell.cardBackgroundColor,
                                            topInset: Appearance.Cell.Inset.normal,
                                            bottomInset: .zero,
                                            textAlignment: .natural,
                                            subtitleLinesCount: 0),
                         selectionAction: { [weak self] _ in
                self?.outputs.didTouchKeyFiguresMap()
            })
        } else {
            return nil
        }
    }

    func keyFigureWarningRow() -> CVRow {
        return CVRow(subtitle: "keyFiguresController.fetchError.message".localized,
                     xibName: .homeStandardCardCell,
                     theme: CVRow.Theme(backgroundColor: Appearance.Cell.cardBackgroundColor,
                                        topInset: Appearance.Cell.Inset.normal,
                                        bottomInset: .zero,
                                        textAlignment: .center,
                                        subtitleLinesCount: 0,
                                        maskedCorners: .top),
                     selectionAction: { _ in
            HUD.show(.progress)
            KeyFiguresManager.shared.fetchKeyFigures {
                HUD.hide()
            }
        })
    }

    func keyFigureRetryRow() -> CVRow {
        return CVRow(title: "keyFiguresController.fetchError.button".localized,
                     xibName: .homeStandardCardCell,
                     theme:  CVRow.Theme(backgroundColor: Appearance.Button.Secondary.backgroundColor,
                                         topInset: .zero,
                                         bottomInset: .zero,
                                         textAlignment: .center,
                                         titleFont: { Appearance.Cell.Text.actionTitleFont },
                                         titleColor: Appearance.Button.Secondary.titleColor,
                                         separatorLeftInset: nil,
                                         separatorRightInset: nil,
                                         maskedCorners: .bottom),
                     selectionAction: { _ in
            HUD.show(.progress)
            KeyFiguresManager.shared.fetchKeyFigures {
                HUD.hide()
            }
        })
    }
}

// MARK: - @objc functions
private extension UIViewController {
    @objc func accessibilityDidActivateChangeLocation() {
        KeyFiguresManager.shared.defineNewPostalCode(from: self)
    }
    
    @objc func accessibilityDidActivateDeleteLocation() {
        KeyFiguresManager.shared.deletePostalCode()
    }
}

// MARK: Feature Info Section
private extension HomeBased {
    func featuredInfoRows() -> [CVRow]? {
        let featuredInfos: [FeaturedInfo] = FeaturedInfoManager.shared.featuredInfos
        let thumbnails: [String: UIImage] = FeaturedInfoManager.shared.thumbnails
        let ressources: [String: URL] = FeaturedInfoManager.shared.ressources
        guard !featuredInfos.isEmpty else { return nil }
        if #available(iOS 13.0, *) {
            return featuredInfos.map { featuredInfo in
                featuredInfoRow(featuredInfo, thumbnail: thumbnails.first(where: { featuredInfo.id == $0.key })?.value, ressource: ressources.first(where: { featuredInfo.id == $0.key })?.value)
            }
        } else {
            let featuredInfo: FeaturedInfo = featuredInfos[0]
            return [featuredInfoRow(featuredInfo, thumbnail: thumbnails.first(where: { featuredInfo.id == $0.key })?.value, ressource: ressources.first(where: { featuredInfo.id == $0.key })?.value, inseted: true)]
        }
    }

    func featuredInfoRow(_ featuredInfo: FeaturedInfo, thumbnail: UIImage?, ressource: URL?, inseted: Bool = false) -> CVRow {
        CVRow(title: featuredInfo.title,
              subtitle: featuredInfo.hint,
              image:  thumbnail ?? UIImage.imageWithColor(color: Appearance.tintSecondaryColor, size: CGSize(width: 1.0, height: 1.0)),
              xibName: .featuredInfoCollectionViewCell,
              theme: .init(backgroundColor: Appearance.Cell.cardBackgroundColor,
                           topInset: .zero,
                           bottomInset: .zero,
                           leftInset: inseted ? Appearance.Cell.Inset.normal : .zero,
                           rightInset: inseted ? Appearance.Cell.Inset.normal : .zero,
                           textAlignment: .natural,
                           titleFont: { Appearance.Cell.Text.titleFont },
                           titleColor: Appearance.Cell.Text.titleColor,
                           titleLinesCount: 2,
                           imageRatio: Appearance.Cell.Image.a4Ratio),
              selectionAction: { [weak self] _ in
            self?.outputs.didTouchFeaturedInfo(featuredInfo, ressource)
        })
    }
}

// MARK: - Info Section
private extension HomeBased {
    
    func infoRows() -> [CVRow]? {
        let infos: [Info] = viewModel.infos
        guard !infos.isEmpty else { return nil }
        if #available(iOS 13.0, *) {
            let infoCells: [CVRow] = infos.prefix(HomeConstant.numberOfDisplayedNews).compactMap { infoRow($0, fractionalWidth: infos.count > 1 ? 0.7 : 1) }
            let seeAllCell: CVRow = .init(title: "home.infoSection.all".localized,
                                          subtitle: "InfoSection", // Use to identify the cell as unique
                                          xibName: .seeAllCardCell,
                                          theme: .init(backgroundColor: Appearance.Button.Secondary.backgroundColor,
                                                       topInset: .zero,
                                                       bottomInset: .zero,
                                                       leftInset: .zero,
                                                       rightInset: .zero,
                                                       textAlignment: .center,
                                                       titleFont: { Appearance.Cell.Text.titleFont },
                                                       titleColor: Appearance.Button.Secondary.titleColor,
                                                       titleLinesCount: 1),
                                          selectionAction: { [weak self] _ in
                self?.outputs.didTouchInfo(nil)
            })
            return infoCells + [seeAllCell]
        } else if let info = infos.first {
            return [infoRow(info, inseted: true)]
        } else {
            return nil
        }
    }

    func infoRow(_ info: Info, inseted: Bool = false, fractionalWidth: CGFloat? = nil) -> CVRow {
        CVRow(title: info.title,
              subtitle: info.description,
              accessoryText: info.formattedDate,
              xibName: .newsCollectionViewCell,
              theme: .init(backgroundColor: Appearance.Cell.cardBackgroundColor,
                           topInset: .zero,
                           bottomInset: .zero,
                           leftInset: inseted ? Appearance.Cell.Inset.normal : .zero,
                           rightInset: inseted ? Appearance.Cell.Inset.normal : .zero,
                           textAlignment: .natural,
                           titleFont: { Appearance.Cell.Text.titleFont },
                           titleLinesCount: 2,
                           subtitleLinesCount: 3,
                           accessoryTextFont: { Appearance.Cell.Text.accessoryFont },
                           accessoryTextColor: Appearance.Cell.Text.accessoryColor),
              selectionAction: { [weak self] _ in
            self?.outputs.didTouchInfo(info)
        }, width: fractionalWidth != nil ? UIScreen.main.bounds.width * fractionalWidth! : nil)
    }

    func usefulLinksRow(inseted: Bool) -> CVRow? {
        guard ParametersManager.shared.displayHomeUsefulLinks else { return nil }
        return CVRow(title: "home.infoSection.usefulLinks".localized,
              subtitle: "home.infoSection.usefulLinks.subtitle".localized,
              image: Asset.Images.useful.image,
              xibName: .usefulLinksCell,
              theme: CVRow.Theme(backgroundColor: Appearance.Cell.cardBackgroundColor,
                                 topInset: inseted ? Appearance.Cell.Inset.normal : .zero,
                                 bottomInset: .zero,
                                 textAlignment: .natural,
                                 titleColor: Appearance.Cell.Text.titleColor,
                                 subtitleColor: Appearance.Cell.Text.titleColor),
              selectionAction: { [weak self] _ in
            self?.outputs.didTouchUsefulLinks()
        })
    }
}

// MARK: - More section
private extension HomeBased {
    func moreRows() -> [CVRow] {
        var menuEntries: [GroupedMenuEntry] = [GroupedMenuEntry(image: Asset.Images.bubble.image,
                                                                title: "home.moreSection.appSharing".localized,
                                                                actionBlock: { [weak self] in
            self?.didTouchShare()
        })]
        
        menuEntries.append(GroupedMenuEntry(image: Asset.Images.manageData.image,
                                            title: "home.moreSection.manageData".localized,
                                            actionBlock: { [weak self] in
            self?.outputs.didTouchManageData()
        }))
        
        menuEntries.append(contentsOf: [GroupedMenuEntry(image: Asset.Images.privacy.image,
                                                         title: "home.moreSection.privacy".localized,
                                                         actionBlock: { [weak self] in
            self?.outputs.didTouchPrivacy()
        }),
                                        GroupedMenuEntry(image: Asset.Images.about.image,
                                                         title: "home.moreSection.aboutStopCovid".localized,
                                                         actionBlock: { [weak self] in
            self?.outputs.didTouchAbout()
        })])
        return menuEntries.toMenuRows(forViewBasedCells: true)
    }
}
