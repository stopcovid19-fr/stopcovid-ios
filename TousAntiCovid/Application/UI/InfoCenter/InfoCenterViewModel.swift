// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
//
//  InfoCenterViewModel.swift
//  TousAntiCovid
//
//  Created by Lunabee Studio / Date - 07/02/2023 - for the TousAntiCovid project.
//

import UseCases
import AppModel
import Protocols

final class InfoCenterViewModel {
    var indexToScrollTo: Int?
    var infos: [Info] { UseCases.infos }
    
    init(indexToScrollTo: Int?) {
        self.indexToScrollTo = indexToScrollTo
    }
    
    func setNewInfoRead() {
        UseCases.setNewInfoRead()
    }
    
    func fetchInfo(forceRefresh: Bool) {
        UseCases.fetchInfo(forceRefresh: forceRefresh)
    }
    
    func addObservers(_ observer: InfoRepositoryObserver) {
        UseCases.addObserverInfo(observer)
    }
    
    func removeObservers(_ observer: InfoRepositoryObserver) {
        UseCases.removeObserverInfo(observer)
    }
}
