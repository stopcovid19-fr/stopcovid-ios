// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
//
//  InfoCenterController.swift
//  TousAntiCovid
//
//  Created by Lunabee Studio / Date - 18/09/2020 - for the TousAntiCovid project.
//

import UIKit
import AppModel
import Protocols

final class InfoCenterController: CVTableViewController {
    private let viewModel: InfoCenterViewModel
    private let deinitBlock: () -> ()

    init(viewModel: InfoCenterViewModel, deinitBlock: @escaping () -> ()) {
        self.viewModel = viewModel
        self.deinitBlock = deinitBlock
        super.init(style: .plain)
    }
    
    required init?(coder: NSCoder) {
        fatalError("Must use default init() method.")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        initUI()
        updateTitle()
        reloadUI()
        viewModel.addObservers(self)
        viewModel.fetchInfo(forceRefresh: true)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        scrollToSelectedIndex()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        viewModel.setNewInfoRead()
    }
    
    deinit {
        viewModel.removeObservers(self)
        deinitBlock()
    }
    
    private func initUI() {
        addHeaderView(height: 10.0)
        tableView.backgroundColor = Appearance.Controller.cardTableViewBackgroundColor
        tableView.showsVerticalScrollIndicator = false
        tableView.separatorStyle = .singleLine
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "common.close".localized, style: .plain, target: self, action: #selector(didTouchCloseButton))
        navigationItem.leftBarButtonItem?.accessibilityHint = "accessibility.closeModal.zGesture".localized
    }
    
    private func scrollToSelectedIndex() {
        guard let index = viewModel.indexToScrollTo else { return }
        let indexPath: IndexPath = .init(row: index, section: 0)
        tableView.scrollToRow(at: indexPath, at: .top, animated: false)
        if index > 0 {
            let previousIndexPath: IndexPath = .init(row: index - 1, section: 0)
            if let cell = tableView.cellForRow(at: indexPath),
               let previousCell = tableView.cellForRow(at: previousIndexPath),
               cell.frame.origin.y >= previousCell.frame.origin.y + previousCell.frame.height {
                viewModel.indexToScrollTo = nil
            }
        } else {
            viewModel.indexToScrollTo = nil
        }
    }
    
    @objc private func didTouchCloseButton() {
        dismiss(animated: true, completion: nil)
    }
    
    private func updateTitle() {
        title = "infoCenterController.title".localized
    }
    
    override func reloadUI(animated: Bool = false, animatedView: UIView? = nil, completion: (() -> ())? = nil) {
        updateEmptyView()
        super.reloadUI(animated: animated, completion: completion)
    }
    
    override func createSections() -> [CVSection] {
        let infos: [Info] = viewModel.infos
        return makeSections {
            if infos.isEmpty {
                CVSection.Empty()
            } else {
                CVSection {
                    infos.map { info in
                        CVRow(title: info.title,
                              subtitle: info.description,
                              accessoryText: info.formattedDate,
                              buttonTitle: info.buttonLabel,
                              xibName: .infoCell,
                              theme: CVRow.Theme(backgroundColor: Appearance.Cell.cardBackgroundColor,
                                                 topInset: Appearance.Cell.Inset.small,
                                                 bottomInset: Appearance.Cell.Inset.small,
                                                 textAlignment: .natural,
                                                 titleFont: { Appearance.Cell.Text.headTitleFont },
                                                 titleHighlightFont: { Appearance.Cell.Text.subtitleBoldFont },
                                                 titleHighlightColor: Appearance.Cell.Text.subtitleColor,
                                                 subtitleFont: { Appearance.Cell.Text.subtitleFont },
                                                 subtitleColor: Appearance.Cell.Text.subtitleColor),
                              associatedValue: info,
                              selectionActionWithCell: { [weak self] view in
                            guard let cell = view as? CVTableViewCell else { return }
                            self?.didTouchSharingFor(cell: cell, info: info)
                        },
                              secondarySelectionAction: {
                            info.url?.openInSafari()
                        })
                    }
                }
            }
        }
    }

    private func updateEmptyView() {
        tableView.backgroundView = viewModel.infos.isEmpty ? InfoCenterEmptyView.view({ [weak self] in self?.viewModel.fetchInfo(forceRefresh: true) }) : nil
    }
    
    private func didTouchSharingFor(cell: CVTableViewCell, info: Info) {
        let sharingText: String = String(format: "info.sharing.title".localized, info.title)
        showSharingController(for: [sharingText])
    }

}

extension InfoCenterController: InfoRepositoryObserver {
    func infoCenterDidUpdate() {
        reloadUI()
    }
}
