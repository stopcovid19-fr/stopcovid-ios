// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
//
//  WalletViewController.swift
//  TousAntiCovid
//
//  Created by Lunabee Studio / Date - 29/10/2020 - for the TousAntiCovid project.
//

import UIKit
import StorageSDK
import PKHUD
import ServerSDK

final class WalletViewController: CVTableViewController {
    
    private enum Mode {
        case empty
        case certificates
        case multiPass
        case info
        
        var selectedIndex: Int {
            switch self {
            case .empty, .certificates: return 0
            case .multiPass: return 1
            case .info: return WalletManager.shared.isMultiPassActivated ? 2 : 1
            }
        }
    }
    
    var deinitBlock: (() -> ())?
    private let didTouchFlashCertificate: () -> ()
    private let didTouchCertificate: (_ certificate: WalletCertificate) -> ()
    private let didTouchDocumentExplanation: (_ certificateType: WalletConstant.CertificateType) -> ()
    private let didTouchMultiPassMoreInfo: () -> ()
    private let didTouchMultiPassInstructions: () -> ()
    private let didTouchCertificateAdditionalInfo: (_ info: AdditionalInfo) -> ()
    private let didSelectProfileForMultiPass: (_ certificates: [EuropeanCertificate]) -> ()
    private var mode: Mode = .empty {
        didSet {
            updateBottomBarButton()
        }
    }
    private var mustScrollToTopAfterRefresh: Bool = false
    private var shouldHideCellImage: Bool {
        UIScreen.main.bounds.width < 375.0 ||
        [.accessibilityLarge,
         .accessibilityExtraLarge,
         .accessibilityExtraExtraLarge,
         .accessibilityExtraExtraExtraLarge,
         .extraLarge,
         .extraExtraLarge,
         .extraExtraExtraLarge].contains(UIApplication.shared.preferredContentSizeCategory)
    }
    private weak var currentlyFocusedCertificate: WalletCertificate?
    private weak var currentlyFocusedCell: CVTableViewCell?

    init(didTouchFlashCertificate: @escaping () -> (),
         didTouchCertificate: @escaping (_ certificate: WalletCertificate) -> (),
         didTouchDocumentExplanation: @escaping (_ certificateType: WalletConstant.CertificateType) -> (),
         didTouchMultiPassMoreInfo: @escaping () -> (),
         didTouchMultiPassInstructions: @escaping () -> (),
         didTouchCertificateAdditionalInfo: @escaping (_ info: AdditionalInfo) -> (),
         didSelectProfileForMultiPass: @escaping (_ certificates: [EuropeanCertificate]) -> (),
         deinitBlock: @escaping () -> ()) {
        self.didTouchFlashCertificate = didTouchFlashCertificate
        self.didTouchCertificate = didTouchCertificate
        self.didTouchDocumentExplanation = didTouchDocumentExplanation
        self.didTouchMultiPassMoreInfo = didTouchMultiPassMoreInfo
        self.didTouchMultiPassInstructions = didTouchMultiPassInstructions
        self.didTouchCertificateAdditionalInfo = didTouchCertificateAdditionalInfo
        self.didSelectProfileForMultiPass = didSelectProfileForMultiPass
        self.deinitBlock = deinitBlock
        super.init(style: .plain)
    }
    
    required init?(coder: NSCoder) {
        fatalError("Must use default init() method.")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        (bottomButtonContainerController ?? self).title = "walletController.title".localized
        initUI()
        reloadUI()
        addObservers()
        updateBottomBarButton()
    }
    
    deinit {
        removeObservers()
        deinitBlock?()
    }
    
    private func initUI() {
        tableView.backgroundColor = Appearance.Controller.cardTableViewBackgroundColor
        tableView.showsVerticalScrollIndicator = false
        tableView.separatorStyle = .singleLine
        let barButtonItem: UIBarButtonItem = UIBarButtonItem(title: "common.close".localized, style: .plain, target: self, action: #selector(didTouchCloseButton))
        barButtonItem.accessibilityHint = "accessibility.closeModal.zGesture".localized
        (bottomButtonContainerController ?? self).navigationItem.leftBarButtonItem = barButtonItem
    }
    
    @objc private func didTouchCloseButton() {
        dismiss(animated: true, completion: nil)
    }
    
    private func addObservers() {
        WalletManager.shared.addObserver(self)
        WalletElgRulesManager.shared.addObserver(self)
        WalletValidityRulesManager.shared.addObserver(self)
        NotificationCenter.default.addObserver(forName: UIContentSizeCategory.didChangeNotification, object: nil, queue: .main) { [weak self] _ in
            self?.reloadUI()
        }
    }
    
    private func removeObservers() {
        WalletManager.shared.removeObserver(self)
        WalletElgRulesManager.shared.removeObserver(self)
        WalletValidityRulesManager.shared.removeObserver(self)
    }
    
    private func updateBottomBarButton() {
        bottomButtonContainerController?.setBottomBarHidden(mode == .info, animated: true)
        switch mode {
        case .multiPass:
            bottomButtonContainerController?.updateButton(title: "multiPass.tab.generation.button.title".localized) { [weak self] in
                self?.openProfilesAlert()
                self?.bottomButtonContainerController?.unlockButtons()
            }
        default:
            bottomButtonContainerController?.updateButton(title: "walletController.addCertificate".localized) { [weak self] in
                self?.openQRScan()
                self?.bottomButtonContainerController?.unlockButtons()
            }
        }
    }
    
    override func createSections() -> [CVSection] {
        mode = calculateNewMode()
        return makeSections {
            if mode != .empty {
                headerSection()
            }
            switch mode {
            case .certificates:
                certificatesSections()
            case .multiPass:
                multiPassInfoSection()
                if UIAccessibility.isVoiceOverRunning {
                    CVSection {
                        addMultiPassRow()
                    }
                }
            case .info, .empty:
                infoSection()
            }
        }
    }
    
    private func calculateNewMode() -> Mode {
        WalletManager.shared.walletCertificates.isEmpty ? .empty : (mode == .empty ? .certificates : mode)
    }
}

// MARK: - Public Actions {
extension WalletViewController {
    func scrollTo(_ certificate: WalletCertificate, animated: Bool = true) {
        if mode != .certificates {
            mode = .certificates
            reloadUI()
        }
        guard let indexPath = getIndexPath(for: certificate) else { return }
        tableView.scrollToRow(at: indexPath, at: .middle, animated: animated)
    }
}

// MARK: - Actions
private extension WalletViewController {
    
    func didTouchCertificateMenuButton(certificate: WalletCertificate, cell: CVTableViewCell) {
        let alertController: UIAlertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        alertController.addAction(UIAlertAction(title: "walletController.menu.share".localized, style: .default, handler: { [weak self] _ in
            self?.showSharingConfirmationController {
                self?.showSanitaryCertificateSharing(image: cell.capture(), text: certificate.fullDescription ?? "")
            }
        }))
        alertController.addAction(UIAlertAction(title: "walletController.menu.delete".localized, style: .destructive, handler: { [weak self] _ in
            self?.showCertificateDeletionAlert(certificate: certificate)
        }))
        alertController.addAction(UIAlertAction(title: "common.cancel".localized, style: .cancel))
        present(alertController, animated: true)
    }
    
    func showSharingConfirmationController(confirmationHandler: @escaping () -> ()) {
        let bottomSheet: BottomSheetAlertController = .init(
            title: "certificateSharingController.title".localized,
            message: "certificateSharingController.message".localized,
            okTitle: "common.confirm".localized,
            cancelTitle: "common.cancel".localized) {
                confirmationHandler()
            }
        bottomSheet.show()
    }
    
    func showCertificateDeletionAlert(certificate: WalletCertificate) {
        showAlert(title: "walletController.menu.delete.alert.title".localized,
                  message: "walletController.menu.delete.alert.message".localized,
                  okTitle: "common.ok".localized,
                  isOkDestructive: true,
                  cancelTitle: "common.cancel".localized, handler: {
            WalletManager.shared.deleteCertificate(id: certificate.id)
        })
    }
    
    func showSanitaryCertificateSharing(image: UIImage?, text: String) {
        showSharingController(for: ["\n\n\("walletController.menu.share.text".localized)", text, image])
    }
    
    func getIndexPath(for certificate: WalletCertificate) -> IndexPath? {
        var indexPath: IndexPath? = nil
        for (index, section) in sections.enumerated() {
            let rowIndex: Int? = section.rows.firstIndex { ($0.associatedValue as? WalletCertificate)?.value == certificate.value }
            guard let row = rowIndex else { continue }
            indexPath = IndexPath(row: row, section: index)
            break
        }
        return indexPath
    }
    
    func openQRScan() {
        CameraAuthorizationManager.requestAuthorization { granted, isFirstTimeRequest in
            if granted {
                self.didTouchFlashCertificate()
            } else if !isFirstTimeRequest {
                self.showAlert(title: "scanCodeController.camera.authorizationNeeded.title".localized,
                               message: "scanCodeController.camera.authorizationNeeded.message".localized,
                               okTitle: "common.settings".localized,
                               cancelTitle: "common.cancel".localized, handler:  {
                    UIApplication.shared.openSettings()
                })
            }
        }
    }
    
    func openProfilesAlert() {
        let profiles: [String: [EuropeanCertificate]] = WalletManager.shared.getWalletMultiPassProfiles()
        if profiles.count > 1 {
            let actions: [UIAlertAction] = profiles.compactMap { profile in
                guard let certificate = profile.value.first else { return nil }
                let name: String = certificate.firstname?.capitalized ?? certificate.lastname?.capitalized ?? ""
                let birthDate: String = Date(timeIntervalSince1970: certificate.birthdate).shortDateFormatted(timeZoneIndependant: true)
                return UIAlertAction(title: name + " (\(birthDate))", style: .default) { [weak self] _ in
                    self?.didSelectProfileForMultiPass(profile.value)
                }
            }.sorted { ($0.title ?? "") < ($1.title ?? "")}
            showActionSheet(title: nil, message: "multiPass.tab.generation.profileList.title".localized, actions: actions, showCancel: true)
        } else if let certificates = profiles.first?.value, profiles.count == 1 {
            didSelectProfileForMultiPass(certificates)
        } else {
            showAlert(title: "multiPass.noProfile.alert.title".localized, message: "multiPass.noProfile.alert.subtitle".localized, okTitle: "common.ok".localized, isOkDestructive: false)
        }
    }

    @objc func accessibilityDidActivateFullscreenAction() {
        guard let certificate = currentlyFocusedCertificate else { return }
        didTouchCertificate(certificate)
    }

    @objc func accessibilityDidActivateFavoriteAction() {
        guard let certificate = currentlyFocusedCertificate else { return }
        if certificate.id == WalletManager.shared.favoriteDccId {
            WalletManager.shared.removeFavorite()
        } else {
            self.mustScrollToTopAfterRefresh = true
            WalletManager.shared.setFavorite(certificate: certificate)
        }
    }

    @objc func accessibilityDidActivateShareAction() {
        guard let certificate = currentlyFocusedCertificate else { return }
        showSanitaryCertificateSharing(image: currentlyFocusedCell?.capture(), text: certificate.fullDescription ?? "")
    }

    @objc func accessibilityDidActivateDeleteAction() {
        guard let certificate = currentlyFocusedCertificate else { return }
        showCertificateDeletionAlert(certificate: certificate)
    }

    func clearCurrentlyFocusedCellObjects() {
        currentlyFocusedCertificate = nil
        currentlyFocusedCell = nil
    }

}

// MARK: - Sections creation
private extension WalletViewController {

    func headerSection() -> CVSection {
        CVSection {
            if UIAccessibility.isVoiceOverRunning {
                addCertificateRow()
            }
            modeSelectionRow()
        }
    }

    func infoSection() -> CVSection {
        CVSection {
            headerImageRow()
            explanationsRow()
            documentsRow()
        }
    }

    func multiPassInfoSection() -> CVSection {
        CVSection {
            let similarProfilesNames: [String] = WalletManager.shared.getSimilarProfileNames()
            if !similarProfilesNames.isEmpty {
                multiPassWarningRow(similarProfilesNames: similarProfilesNames)
            }
            multiPassExplanationsRow()
        }
    }

    func certificatesSections() -> [CVSection] {
        makeSections {
            if WalletManager.shared.areThereCertificatesNeedingAttention {
                needingAttentionSection()
            }
            let sections: [WalletManager.WalletCertificateSection: [WalletCertificate]] = WalletManager.shared.sections

            favoriteCertificateSection(favoriteCertificate: sections[.favorite]?.first)

            let validCertificates: [WalletCertificate] = sections[.valid] ?? []
            if !validCertificates.isEmpty {
                certificateSection(title: "walletController.valid.title".localized, certificates: validCertificates)
            }

            let validSoonCertificates: [WalletCertificate] = sections[.validSoon] ?? []
            if !validSoonCertificates.isEmpty {
                certificateSection(title: "walletController.validSoon.title".localized, certificates: validSoonCertificates)
            }

            let negativeTestsCertificates: [WalletCertificate] = sections[.negativeTest] ?? []
            if !negativeTestsCertificates.isEmpty {
                certificateSection(title: "walletController.negTests.title".localized, certificates: negativeTestsCertificates)
            }

            let othersCertificates: [WalletCertificate] = sections[.other] ?? []
            if !othersCertificates.isEmpty {
                certificateSection(title: "walletController.others.title".localized, certificates: othersCertificates)
            }

            let expiredCertificates: [WalletCertificate] = sections[.expired] ?? []
            if !expiredCertificates.isEmpty {
                certificateSection(title: "walletController.expired.title".localized, certificates: expiredCertificates)
            }
        }
    }

    func needingAttentionSection() -> CVSection {
        CVSection {
            CVRow(title: nil,
                  subtitle: "walletController.certificateWarning".localized,
                  xibName: .textCell,
                  theme: CVRow.Theme(topInset: Appearance.Cell.Inset.medium,
                                     bottomInset: .zero,
                                     textAlignment: .natural),
                  accessibilityDidFocusCell: { [weak self] _ in
                self?.clearCurrentlyFocusedCellObjects()
            })
        }
    }

    func favoriteCertificateSection(favoriteCertificate: WalletCertificate?) -> CVSection {
        let favoriteEmptyText: String = "walletController.favoriteCertificateSection.subtitle".localized
        return CVSection(title: "walletController.favoriteCertificateSection.title".localized,
                         subtitle: favoriteCertificate == nil ? favoriteEmptyText : nil) {
            if let certificate = favoriteCertificate {
                certificateRows(certificate: certificate, isLast: true)
            }
            if #available(iOS 14.0, *) {
                favoriteCertificateWidgetRow(hasFavoriteCertificate: favoriteCertificate != nil)
            }
        }
    }

    func certificateSection(title: String, certificates: [WalletCertificate]) -> CVSection {
        CVSection(title: title) {
            var certificatesRows: [CVRow] = certificates.sorted { $0.timestamp > $1.timestamp }.flatMap { certificateRows(certificate: $0) }
            certificatesRows.indices.last.map { certificatesRows[$0].theme.bottomInset = .zero }
            return certificatesRows
        }
    }
}

// MARK: - Rows creation
private extension WalletViewController {

    func addCertificateRow() -> CVRow {
        CVRow(title: "walletController.addCertificate".localized,
              xibName: .buttonCell,
              theme: CVRow.Theme(topInset: Appearance.Cell.Inset.large,
                                 bottomInset: .zero,
                                 buttonStyle: .primary),
              selectionAction: { [weak self] _ in
            self?.openQRScan()
        })
    }

    func addMultiPassRow() -> CVRow {
        CVRow(title: "multiPass.tab.generation.button.title".localized,
              xibName: .buttonCell,
              theme: CVRow.Theme(topInset: Appearance.Cell.Inset.large,
                                 bottomInset: .zero,
                                 buttonStyle: .primary),
              selectionAction: { [weak self] _ in
            self?.openProfilesAlert()
        })
    }


    func modeSelectionRow() -> CVRow {
        let certificatesModeSelectionAction: () -> () = { [weak self] in
            self?.mode = .certificates
            self?.reloadUI(animated: true, completion: nil)
        }
        let multiPassModeSelectionAction: (() -> ())? = WalletManager.shared.isMultiPassActivated ? { [weak self] in
            self?.mode = .multiPass
            self?.reloadUI(animated: true, completion: nil)
        } : nil
        let infoModeSelectionAction: () -> () = { [weak self] in
            self?.mode = .info
            self?.reloadUI(animated: true, completion: nil)
        }
        return CVRow(segmentsTitles: ["walletController.mode.myCertificates".localized,
                                       WalletManager.shared.isMultiPassActivated ? "multiPass.tab.title".localized : nil,
                                       "walletController.mode.info".localized].compactMap { $0 },
                      selectedSegmentIndex: mode.selectedIndex,
                      xibName: .segmentedCell,
                      theme:  CVRow.Theme(backgroundColor: .clear,
                                          topInset: Appearance.Cell.Inset.normal,
                                          bottomInset: 1.0, // cannot be .zero else bottom line is truncated...
                                          textAlignment: .natural,
                                          titleFont: { Appearance.SegmentedControl.selectedFont },
                                          subtitleFont: { Appearance.SegmentedControl.normalFont }),
                      accessibilityDidFocusCell: { [weak self] _ in
            self?.clearCurrentlyFocusedCellObjects()
        },
                      segmentsActions: [certificatesModeSelectionAction, multiPassModeSelectionAction, infoModeSelectionAction].compactMap { $0 })
    }

    func favoriteCertificateWidgetRow(hasFavoriteCertificate: Bool) -> CVRow {
        CVRow(subtitle: "walletController.favoriteCertificateSection.widget.ios".localized,
              xibName: .textCell,
              theme: CVRow.Theme(topInset: hasFavoriteCertificate ? Appearance.Cell.Inset.medium : .zero,
                                 bottomInset: .zero,
                                 textAlignment: .natural),
              accessibilityDidFocusCell: { [weak self] _ in
            self?.clearCurrentlyFocusedCellObjects()
        })
    }

    func headerImageRow() -> CVRow {
        CVRow(image: Asset.Images.wallet.image,
              xibName: .imageCell,
              theme: CVRow.Theme(topInset: Appearance.Cell.Inset.medium,
                                 imageRatio: 375.0 / 116.0))
    }

    func explanationsRow() -> CVRow {
        CVRow(title: "walletController.howDoesItWork.title".localized,
              subtitle: "walletController.howDoesItWork.subtitle".localized,
              xibName: .standardCardCell,
              theme: CVRow.Theme(backgroundColor: Appearance.Cell.cardBackgroundColor,
                                 topInset: .zero,
                                 bottomInset: .zero,
                                 textAlignment: .natural,
                                 titleFont: { Appearance.Cell.Text.headTitleFont }),
              accessibilityDidFocusCell: { [weak self] _ in
            self?.clearCurrentlyFocusedCellObjects()
        })
    }
    func documentsRow() -> CVRow {
        CVRow(title: "walletController.documents.title".localized,
              subtitle: "walletController.documents.subtitle".localized,
              accessoryText: "walletController.documents.vaccin".localized,
              footerText: "walletController.documents.test".localized,
              image: WalletImagesManager.shared.image(named: .vaccinEuropeCertificate),
              secondaryImage: WalletImagesManager.shared.image(named: .testEuropeCertificate),
              xibName: .walletDocumentsCell,
              theme: CVRow.Theme(backgroundColor: Appearance.Cell.cardBackgroundColor,
                                 topInset: Appearance.Cell.Inset.normal,
                                 bottomInset: .zero,
                                 textAlignment: .natural,
                                 titleFont: { Appearance.Cell.Text.headTitleFont }),
              accessibilityDidFocusCell: { [weak self] _ in
            self?.clearCurrentlyFocusedCellObjects()
        },
              secondarySelectionAction: { [weak self] in
            self?.didTouchDocumentExplanation(.vaccinationEurope)
        },
              tertiarySelectionAction: { [weak self] in
            self?.didTouchDocumentExplanation(.sanitaryEurope)
        })
    }

    func multiPassExplanationsRow() -> CVRow {
        let url: URL? = URL(string: "multiPass.tab.explanation.url".localized)
        return CVRow(title: "multiPass.tab.explanation.title".localized,
                     subtitle: "multiPass.tab.explanation.subtitle".localized,
                     buttonTitle: url == nil ? nil : "multiPass.tab.explanation.linkButton.title".localized,
                     xibName: .paragraphCell,
                     theme: CVRow.Theme(backgroundColor: Appearance.Cell.cardBackgroundColor,
                                        topInset: Appearance.Cell.Inset.normal,
                                        bottomInset: .zero,
                                        textAlignment: .natural,
                                        titleFont: { Appearance.Cell.Text.headTitleFont }),
                     accessibilityDidFocusCell: { [weak self] _ in
            self?.clearCurrentlyFocusedCellObjects()
        },
                     secondarySelectionAction: url == nil ? nil : { [weak self] in
            self?.didTouchMultiPassMoreInfo()
        })
    }

    func multiPassWarningRow(similarProfilesNames: [String]) -> CVRow {
        let instructionUrl: URL? = URL(string: "multiPass.tab.similarProfile.url".localized)
        return CVRow(title: "multiPass.tab.similarProfile.title".localized,
                     subtitle: String(format: "multiPass.tab.similarProfile.subtitle".localized, similarProfilesNames.joined(separator: ", ")),
                     buttonTitle: instructionUrl == nil ? nil : "multiPass.tab.similarProfile.linkButton.title".localized,
                     xibName: .paragraphCell,
                     theme: CVRow.Theme(backgroundColor: Appearance.Cell.cardBackgroundColor,
                                        topInset: Appearance.Cell.Inset.normal,
                                        bottomInset: .zero,
                                        textAlignment: .natural,
                                        titleFont: { Appearance.Cell.Text.headTitleFont }),
                     accessibilityDidFocusCell: { [weak self] _ in
            self?.clearCurrentlyFocusedCellObjects()
        },
                     secondarySelectionAction: instructionUrl == nil ? nil : { [weak self] in
            self?.didTouchMultiPassInstructions()
        })
    }

    func additionalInfoRowIfNeeded(for certificate: WalletCertificate) -> CVRow? {
        var additionalInfo: [AdditionalInfo] = certificate.additionalInfo

        if WalletManager.shared.shouldUseSmartWallet,
           let europeanCertificate = certificate as? EuropeanCertificate,
           WalletManager.shared.isRelevantCertificate(europeanCertificate) {
            if let desc = WalletManager.shared.expirationDescription(for: europeanCertificate) {
                additionalInfo.append(AdditionalInfo(category: .error, fullDescription: desc))
            } else if let desc = WalletManager.shared.expirationSoonDescription(for: europeanCertificate) {
                additionalInfo.append(AdditionalInfo(category: .warning, fullDescription: desc))
            } else if let desc = WalletManager.shared.eligibilityDescription(for: europeanCertificate) {
                additionalInfo.append(AdditionalInfo(category: .info, fullDescription: desc))
            } else if let desc = WalletManager.shared.eligibleSoonDescription(for: europeanCertificate) {
                additionalInfo.append(AdditionalInfo(category: .info, fullDescription: desc))
            }
        }

        if !additionalInfo.errors.isEmpty {
            let aggregateDescription: String = additionalInfo.errors.map { $0.fullDescription }.joined(separator: "\n\n")
            return row(for: aggregateDescription, additionalInfoCategory: .error)
        } else if !additionalInfo.warnings.isEmpty {
            let aggregateDescription: String = additionalInfo.warnings.map { $0.fullDescription }.joined(separator: "\n\n")
            return row(for: aggregateDescription, additionalInfoCategory: .warning)
        } else if !additionalInfo.info.isEmpty {
            let aggregateDescription: String = additionalInfo.info.map { $0.fullDescription }.joined(separator: "\n\n")
            return row(for: aggregateDescription, additionalInfoCategory: .info)
        }
        return nil
    }

    func row(for additionalInfoDesc: String, additionalInfoCategory: AdditionalInfo.Category) -> CVRow {
        let backgroundColor: UIColor = additionalInfoCategory.backgroundColor
        let image: UIImage
        let titleColor: UIColor
        switch additionalInfoCategory {
        case .error:
            image = Asset.Images.passWarning.image
            titleColor = .white.withAlphaComponent(0.85)
        case .warning:
            image = Asset.Images.passWarning.image
            titleColor = .black.withAlphaComponent(0.55)
        case .info:
            image = Asset.Images.passInfo.image
            titleColor = .white.withAlphaComponent(0.85)
        }

        return CVRow(title: additionalInfoDesc,
                     subtitle: "common.readNext".localized,
                     image: shouldHideCellImage ? nil : image,
                     xibName: .additionalInfoCell,
                     theme: CVRow.Theme(backgroundColor: backgroundColor,
                                        topInset: .zero,
                                        bottomInset: .zero,
                                        textAlignment: .natural,
                                        titleFont: { Appearance.Cell.Text.subtitleFont },
                                        titleColor: titleColor,
                                        titleLinesCount: 2,
                                        subtitleFont: { Appearance.Cell.Text.accessoryFont },
                                        subtitleColor: titleColor,
                                        imageSize: Appearance.Cell.Image.size,
                                        interLabelSpacing: Appearance.Cell.Inset.normal,
                                        maskedCorners: .top),
                     selectionAction: { [weak self] cell in
            if cell?.cvSubtitleLabel?.isHidden == false {
                let additionalInfo: AdditionalInfo = .init(category: additionalInfoCategory, fullDescription: additionalInfoDesc)
                self?.didTouchCertificateAdditionalInfo(additionalInfo)
            }
        }, willDisplay: { cell in
            (cell as? AdditionalInfoCell)?.cvImageView?.alpha = additionalInfoCategory == .warning ? 0.55 : 0.85
        }, didValidateValue: { [weak self] _, cell in
            self?.tableView.beginUpdates()
            cell.layoutSubviews()
            self?.tableView.endUpdates()
        })
    }

    func certificateRows(certificate: WalletCertificate, isLast: Bool = false) -> [CVRow] {
        var rows: [CVRow] = []
        var subtitle: String = certificate.fullDescription ?? ""
        if let euroCertificate = certificate as? EuropeanCertificate, !WalletManager.shared.isCertificateEuValidated(euroCertificate) {
            subtitle.append("\n \("wallet.proof.europe.test.negative.tagNotUe".localized)")
        }
        if let row = additionalInfoRowIfNeeded(for: certificate) {
            rows.append(row)
        }
        let canBeAddedToFavorite: Bool = certificate.type.format == .walletDCC || certificate.type == .multiPass
        let certificateRow: CVRow = CVRow(title: certificate.shortDescriptionForList?.uppercased(),
                                          subtitle: subtitle,
                                          image: shouldHideCellImage ? nil : certificate.codeImage,
                                          isOn: certificate.id == WalletManager.shared.favoriteDccId,
                                          xibName: .sanitaryCertificateCell,
                                          theme: CVRow.Theme(backgroundColor: Appearance.Cell.cardBackgroundColor,
                                                             topInset: .zero,
                                                             bottomInset: UIAccessibility.isVoiceOverRunning ? Appearance.Cell.Inset.medium : .zero,
                                                             textAlignment: shouldHideCellImage ? .center : .natural,
                                                             titleFont: { Appearance.Cell.Text.smallHeadTitleFont },
                                                             titleColor: Appearance.Cell.Text.titleColor,
                                                             subtitleFont: { Appearance.Cell.Text.subtitleFont },
                                                             accessoryTextFont: { Appearance.Cell.Text.subtitleFont },
                                                             imageSize: Appearance.Cell.Image.largeSize,
                                                             maskedCorners: UIAccessibility.isVoiceOverRunning ? (rows.isEmpty ? .all : .bottom) : (rows.isEmpty ? .top : .none)),
                                          associatedValue: certificate,
                                          accessibilityDidFocusCell: { [weak self] cell in
            self?.currentlyFocusedCertificate = certificate
            self?.currentlyFocusedCell = cell
        },
                                          selectionActionWithCell: { [weak self] cell in
            guard let cell = cell as? CVTableViewCell else { return }
            self?.didTouchCertificateMenuButton(certificate: certificate, cell: cell)
        },
                                          selectionAction: { [weak self] _ in
            self?.didTouchCertificate(certificate)
        },
                                          secondarySelectionAction: canBeAddedToFavorite ? { [weak self] in
            guard let self = self else { return }
            if certificate.id == WalletManager.shared.favoriteDccId {
                WalletManager.shared.removeFavorite()
            } else {
                self.mustScrollToTopAfterRefresh = true
                WalletManager.shared.setFavorite(certificate: certificate)
            }
        } : nil,
                                          willDisplay: { [weak self] cell in
            guard let self = self else { return }
            let isFavorite: Bool = certificate.id == WalletManager.shared.favoriteDccId
            let customActions: [UIAccessibilityCustomAction] = [
                UIAccessibilityCustomAction(name: "walletController.favoriteCertificateSection.openFullScreen".localized, target: self, selector: #selector(self.accessibilityDidActivateFullscreenAction)),
                UIAccessibilityCustomAction(name: "accessibility.wallet.dcc.favorite.\(isFavorite ? "remove" : "define")".localized, target: self, selector: #selector(self.accessibilityDidActivateFavoriteAction)),
                UIAccessibilityCustomAction(name: "walletController.menu.share".localized, target: self, selector: #selector(self.accessibilityDidActivateShareAction)),
                UIAccessibilityCustomAction(name: "walletController.menu.delete".localized, target: self, selector: #selector(self.accessibilityDidActivateDeleteAction))
            ]
            cell.accessibilityCustomActions = customActions
        })
        rows.append(certificateRow)
        if !UIAccessibility.isVoiceOverRunning {
            let actionRow: CVRow = CVRow(title: "walletController.favoriteCertificateSection.openFullScreen".localized,
                                         xibName: .standardCardCell,
                                         theme:  CVRow.Theme(backgroundColor: Appearance.Button.Secondary.backgroundColor,
                                                             topInset: .zero,
                                                             bottomInset: Appearance.Cell.Inset.medium,
                                                             textAlignment: .center,
                                                             titleFont: { Appearance.Cell.Text.actionTitleFont },
                                                             titleColor: Appearance.Button.Secondary.titleColor,
                                                             separatorLeftInset: nil,
                                                             separatorRightInset: nil,
                                                             maskedCorners: .bottom),
                                         accessibilityDidFocusCell: { [weak self] _ in
                self?.clearCurrentlyFocusedCellObjects()
            },
                                         selectionAction: { [weak self] _ in
                self?.didTouchCertificate(certificate)
            })
            rows.append(actionRow)
        }
        if isLast {
            rows.indices.last.map { rows[$0].theme.bottomInset = .zero }
        }
        return rows
    }
}

extension WalletViewController: WalletChangesObserver {
    
    func walletCertificatesDidUpdate() {
        mode = calculateNewMode()
        reloadUI(animated: true)
    }

    func walletFavoriteCertificateDidUpdate() {
        reloadUI(animated: false) { [weak self] in
            guard self?.mustScrollToTopAfterRefresh == true else { return }
            self?.mustScrollToTopAfterRefresh = false
            self?.tableView.scrollToTop()
        }
    }
    
    func walletSmartStateDidUpdate() {}
    
    func walletBlacklistDidUpdate() {
        reloadUI(animated: true)
    }
}

extension WalletViewController: WalletRulesObserver {
    func walletRulesDidUpdate() {
        reloadUI(animated: true)
    }
}
