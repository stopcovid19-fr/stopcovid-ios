// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
//
//  UserLanguageController.swift
//  TousAntiCovid
//
//  Created by Lunabee Studio / Date - 19/07/2021 - for the TousAntiCovid project.
//

import UIKit
import AppModel

final class UserLanguageController: CVTableViewController {

    private let viewModel: UserLanguageViewModel
    private var deinitBlock: (() -> ())?
    
    init(viewModel: UserLanguageViewModel, deinitBlock: @escaping () -> ()) {
        self.deinitBlock = deinitBlock
        self.viewModel = viewModel
        super.init(style: .plain)
    }
    
    required init?(coder: NSCoder) {
        self.viewModel = UserLanguageViewModel()
        super.init(coder: coder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        updateTitle()
        initUI()
        reloadUI()
        LocalizationsManager.shared.addObserver(self)
    }
    
    deinit {
        LocalizationsManager.shared.removeObserver(self)
        deinitBlock?()
    }
    
    override func createSections() -> [CVSection] {
        makeSections {
            CVSection {
                CVRow(title: "userLanguageController.subtitle".localized,
                      xibName: .textCell,
                      theme: CVRow.Theme(topInset: Appearance.Cell.Inset.normal,
                                         bottomInset: Appearance.Cell.Inset.normal,
                                         textAlignment: .natural,
                                         titleColor: Appearance.tintColor,
                                         separatorLeftInset: .zero,
                                         separatorRightInset: .zero))
                selectableRow(title: "manageDataController.languageFR".localized,
                              isSelected: viewModel.currentAppLanguage == Language.french,
                              selectionBlock: { [weak self] in
                    self?.viewModel.setCurrentAppLanguage(to: .french)
                })
                selectableRow(title: "manageDataController.languageEN".localized,
                              isSelected: viewModel.currentAppLanguage == Language.english,
                              selectionBlock: { [weak self] in
                    self?.viewModel.setCurrentAppLanguage(to: .english)
                })
                CVRow(title: "userLanguageController.footer".localized,
                      xibName: .textCell,
                      theme: CVRow.Theme(topInset: Appearance.Cell.Inset.normal,
                                         bottomInset: Appearance.Cell.Inset.normal,
                                         textAlignment: .natural,
                                         titleFont: { Appearance.Cell.Text.subtitleFont },
                                         titleColor: Appearance.Cell.Text.placeholderColor,
                                         separatorLeftInset: Appearance.Cell.leftMargin))
                CVRow(title: "userLanguageController.button.title".localized,
                      xibName: .buttonCell,
                      theme: CVRow.Theme(topInset: .zero,
                                         bottomInset: .zero,
                                         buttonStyle: .primary),
                      selectionAction: { [weak self] _ in
                    guard let self = self, self.viewModel.currentAppLanguage.isNil else { return }
                    self.viewModel.setCurrentAppLanguage(to: .english)
                    self.dismiss(animated: true, completion: nil)
                })
            }
        }
    }
    
    private func selectableRow(title: String, isSelected: Bool, selectionBlock: @escaping () -> ()) -> CVRow {
        CVRow(title: title,
              isOn: isSelected,
              xibName: .selectableCell,
              theme: CVRow.Theme(backgroundColor: Appearance.Cell.cardBackgroundColor,
                                 topInset: Appearance.Cell.Inset.normal,
                                 bottomInset: Appearance.Cell.Inset.normal,
                                 textAlignment: .natural,
                                 titleFont: { Appearance.Cell.Text.standardFont },
                                 separatorLeftInset: Appearance.Cell.leftMargin,
                                 separatorRightInset: .zero),
              selectionAction: { _ in
            selectionBlock()
        })
    }
    
    private func updateTitle() {
        title = "userLanguageController.title".localized
    }
    
    private func initUI() {
        tableView.backgroundColor = Appearance.Controller.cardTableViewBackgroundColor
        tableView.showsVerticalScrollIndicator = false
    }
    
}

extension UserLanguageController: LocalizationsChangesObserver {
    
    func localizationsChanged() {
        updateTitle()
        reloadUI()
    }
    
}

