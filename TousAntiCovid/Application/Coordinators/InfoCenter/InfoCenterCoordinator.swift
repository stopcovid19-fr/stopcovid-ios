// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
//
//  InfoCenterCoordinator.swift
//  TousAntiCovid
//
//  Created by Lunabee Studio / Date - 09/04/2020 - for the TousAntiCovid project.
//

import UIKit
import UseCases
import Protocols

final class InfoCenterCoordinator: Coordinator {

    weak var parent: Coordinator?
    var childCoordinators: [Coordinator] = []
    
    private weak var navigationController: UINavigationController?
    private weak var presentingController: UIViewController?
    
    init(presentingController: UIViewController?, parent: Coordinator, showInfoAtIndex: Int? = nil) {
        self.presentingController = presentingController
        self.parent = parent
        start(scrollToIndex: showInfoAtIndex)
    }
    
    private func start(scrollToIndex: Int?) {
        let viewModel: InfoCenterViewModel = .init(indexToScrollTo: scrollToIndex)
        let navigationController: CVNavigationController = CVNavigationController(rootViewController: InfoCenterController(viewModel: viewModel, deinitBlock: { [weak self] in
            self?.didDeinit()
        }))
        self.navigationController = navigationController
        presentingController?.present(navigationController, animated: true)
    }
    
}
