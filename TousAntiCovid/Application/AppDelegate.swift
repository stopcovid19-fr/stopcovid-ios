// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
//
//  AppDelegate.swift
//  TousAntiCovid
//
//  Created by Lunabee Studio / Date - 07/04/2020 - for the TousAntiCovid project.
//

import UIKit
import PKHUD
import StorageSDK
import ServerSDK
import AVFoundation
import UseCases
import struct DependenciesInjection.Register
import Protocols
import Repositories
import AppModel

@UIApplicationMain
final class AppDelegate: UIResponder, UIApplicationDelegate {

    private let rootCoordinator: RootCoordinator = RootCoordinator()

    @UserDefault(key: .isAppAlreadyInstalled)
    var isAppAlreadyInstalled: Bool = false

    @UserDefault(key: .lastBuildNumber)
    var lastBuildNumber: Int = 0

    @UserDefault(key: .isOnboardingDone)
    private var isOnboardingDone: Bool = false

    @UserDefault(key: .lastConfigFetchTimestamp)
    private var lastConfigFetchTimestamp: Double = 0.0

    // MARK: - Dependencies Injection on repositories -
    @Register private var repositories: Repositories
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        initExceptionsCatching()
        initAudioSession()
        initAppearance()
        initUrlCache()
        NotificationsManager.shared.start()
        LocalizationsManager.shared.start()
        FeaturedInfoManager.shared.start()
        ParametersManager.shared.start(configUrl: Constant.Server.configUrl,
                                       configCertificateFiles: Constant.Server.certificates) { task, responseData, error in
        }
        WalletValidityRulesManager.shared.start()
        WalletElgRulesManager.shared.start()
        EUTagsManager.shared.start()
        KeyFiguresManager.shared.start()
        if #available(iOS 13.0, *) { KeyFiguresMapManager.shared.start() }
        let storageManager: StorageManager = StorageManager { message in
            StackLogger.log(symbols: Thread.callStackSymbolsString, message: message)
        }
        storageManager.start()
        StorageAlertManager.shared.start(with: storageManager)
        BlacklistManager.current.start(storageManager: storageManager)
        WalletManager.shared.start(storageManager: storageManager)
        WalletManager.shared.addObserver(self)
        PrivacyManager.shared.start()
        LinksManager.shared.start()
        KeyFiguresExplanationsManager.shared.start()
        WalletImagesManager.shared.start()
        DccCertificatesManager.shared.start()
        OrientationManager.shared.start()
        MultiPassServer.shared.start(session: UrlSessionManager.shared.session,
                                     serverUrl: { Constant.Server.multiPassAggregateUrl },
                                     requestLoggingHandler: { request, response, responseData, error in
        })
        RatingsManager.shared.start()
        if #available(iOS 14.0, *) {
            WidgetDCCManager.shared.start()
        }

        if Int(UIApplication.shared.buildNumber) != lastBuildNumber {
            writeInitialFilesOnAppUpdate()
            manageMigration(lastBuildNumber: lastBuildNumber, storageManager: storageManager)
            isAppAlreadyInstalled = true
            lastBuildNumber = Int(UIApplication.shared.buildNumber) ?? lastBuildNumber
        }
        rootCoordinator.start()
        initAppMaintenance()
        if #available(iOS 12, *) {
            NetworkMonitor.shared.start()
            NetworkAlertManager.shared.start()
        }
        DeepLinkingManager.shared.start()
        HUD.leadingMargin = 8.0
        HUD.trailingMargin = 8.0
        WatchConnectivityManager.shared.start()
        UIApplication.shared.registerForRemoteNotifications()
        if let shortcutItem = launchOptions?[UIApplication.LaunchOptionsKey.shortcutItem] as? UIApplicationShortcutItem {
            processShortcutItem(shortcutItem)
        }
        return true
    }
    
    func application(_ application: UIApplication, performActionFor shortcutItem: UIApplicationShortcutItem, completionHandler: @escaping (Bool) -> Void) {
        processShortcutItem(shortcutItem)
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        let shouldFetchConfig: Bool = lastConfigFetchTimestamp + Constant.timeIntervalToFetchConfig < Date().timeIntervalSince1970
        if shouldFetchConfig {
            ParametersManager.shared.fetchConfig { result in
                switch result {
                case .success:
                    self.lastConfigFetchTimestamp = Date().timeIntervalSince1970
                    self.updateAppShortcut()
                    NotificationCenter.default.post(name: .didFetchConfig, object: nil)
                case .failure:
                    break
                }
            }
        }
        
        UIApplication.shared.clearBadge()
        UseCases.fetchInfo()
        UseCases.fetchMyHealthData()
    }

    func application(_ application: UIApplication, performFetchWithCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        UseCases.fetchInfo()
        KeyFiguresManager.shared.fetchKeyFigures()
        showSmartNotificationIfNeeded()
    }

    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        UseCases.fetchInfo()
        KeyFiguresManager.shared.fetchKeyFigures()
        showSmartNotificationIfNeeded()
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        DeepLinkingManager.shared.processUrl(url)
        return true
    }
    
    func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([UIUserActivityRestoring]?) -> Void) -> Bool {
        if #available(iOS 14.0, *) {
            WidgetDCCManager.shared.processUserActivity(userActivity)
        }
        DeepLinkingManager.shared.appLaunchedFromDeeplinkOrShortcut = true
        DeepLinkingManager.shared.processActivity(userActivity)
        return true
    }
    
    func application(_ application: UIApplication, willContinueUserActivityWithType userActivityType: String) -> Bool {
        return true
    }
    
    func application(_ application: UIApplication, handleEventsForBackgroundURLSession identifier: String, completionHandler: @escaping () -> Void) {
        completionHandler()
    }
    
    private func initAppearance() {
        UINavigationBar.appearance().tintColor = Asset.Colors.tint.color
        UINavigationBar.appearance().titleTextAttributes = [.font: Appearance.NavigationBar.titleFont]
        //Fix Nav Bar tint issue in iOS 15.0 or later - is transparent w/o code below
        if #available(iOS 13, *) {
            let appearance: UINavigationBarAppearance = .init()
            appearance.titleTextAttributes = [.font: Appearance.NavigationBar.titleFont]
            appearance.configureWithDefaultBackground()
            UINavigationBar.appearance().scrollEdgeAppearance = appearance
            UINavigationBar.appearance().standardAppearance = appearance
        }
    }
    
    private func initUrlCache() {
        URLCache.shared.diskCapacity = 0
        URLCache.shared.memoryCapacity = 0
        URLSession.shared.configuration.requestCachePolicy = .reloadIgnoringLocalAndRemoteCacheData
    }
    
    private func initAppMaintenance() {
        MaintenanceManager.shared.start(coordinator: rootCoordinator)
    }

    private func initAudioSession() {
        let audioSession: AVAudioSession = AVAudioSession.sharedInstance()
        do {
            try audioSession.setCategory(.playback, mode: .moviePlayback, options: [.mixWithOthers])
        } catch {}
    }
    
    private func updateAppShortcut() {
        var shortcuts: [UIApplicationShortcutItem] = []
        let qrScanShortcut: UIApplicationShortcutItem = .init(type: Constant.ShortcutItem.qrScan.rawValue,
                                                              localizedTitle: Constant.ShortcutItem.qrScan.rawValue.localized,
                                                              localizedSubtitle: nil,
                                                              icon: UIApplicationShortcutIcon(templateImageName: "qrScan"))
        shortcuts.append(qrScanShortcut)
        if !WalletManager.shared.favoriteDccId.isNil {
            let favoriteDccShortcut: UIApplicationShortcutItem = .init(type: Constant.ShortcutItem.favoriteDcc.rawValue,
                                                                       localizedTitle: Constant.ShortcutItem.favoriteDcc.rawValue.localized,
                                                                       localizedSubtitle: nil,
                                                                       icon: UIApplicationShortcutIcon(templateImageName: "FilledHeart"))
            shortcuts.append(favoriteDccShortcut)
        }

        let warningDeleteAppShortcut: UIApplicationShortcutItem = .init(type: Constant.ShortcutItem.warningDeleteApp.rawValue,
                                                                        localizedTitle: Constant.ShortcutItem.warningDeleteApp.rawValue.localized,
                                                                        localizedSubtitle: "\(Constant.ShortcutItem.warningDeleteApp.rawValue).subtitle".localizedOrNil,
                                                                        icon: UIApplicationShortcutIcon(templateImageName: "PassInfo"))
        shortcuts.append(warningDeleteAppShortcut)

        UIApplication.shared.shortcutItems = shortcuts
    }
    
    private func processShortcutItem(_ shortcutItem: UIApplicationShortcutItem) {
        DeepLinkingManager.shared.appLaunchedFromDeeplinkOrShortcut = true
        if shortcutItem.type == Constant.ShortcutItem.qrScan.rawValue {
            DeepLinkingManager.shared.processOpenQrScan()
        } else if shortcutItem.type == Constant.ShortcutItem.favoriteDcc.rawValue {
            DeepLinkingManager.shared.processOpenFavoriteCertificateQrCode()
        } else if shortcutItem.type == Constant.ShortcutItem.warningDeleteApp.rawValue {
            NotificationCenter.default.post(name: .openWallet, object: nil)
        }
    }

    private func initExceptionsCatching() {
        NSSetUncaughtExceptionHandler { StackLogger.log(exception: $0) }
    }

    private func showSmartNotificationIfNeeded() {
        guard WalletManager.shared.shouldUseSmartWalletNotifications else { return }
        WalletManager.shared.showSmartNotificationIfNeeded()
    }

    private func writeInitialFilesOnAppUpdate() {
        try? UseCases.writeInitialFiles()
    }

    private func manageMigration(lastBuildNumber: Int, storageManager: StorageManager) {
        guard isAppAlreadyInstalled else {
            return
        }
        if lastBuildNumber < 1344 {
            let oldAppLanguageKey: String = "appLanguage"
            if let appLanguageCode = UserDefaults.standard.string(forKey: oldAppLanguageKey) {
                if let appLanguage = Language(rawValue: appLanguageCode) {
                    UseCases.setCurrentAppLanguage(to: appLanguage)
                }
            }
            storageManager.clearKeychain(includingDBKey: false)
            var userDefaultToClean: [String] = ["lastRiskLevelsUpdateDate",
                                                "lastInitialRiskLevelsBuildNumber",
                                                "lastInitialAttestationFormBuildNumber",
                                                "saveAttestationFieldsData",
                                                "didAlreadySeeVenuesRecordingOnboarding",
                                                "venuesFeaturedWasActivatedAtLeastOneTime",
                                                "hideStatus",
                                                "installationUuid",
                                                "lastProximityActivationStartTimestamp",
                                                "cleaLastIteration",
                                                "isAnalyticsOptIn",
                                                "deleteAnalyticsAfterNextStatus",
                                                "appLanguage",
                                                "activityPassAutoRenewalActivated",
                                                "activityPassNotificationActivated",
                                                "hideStatusBar",
                                                "didAlreadyStartBluetoothOneTime"]
            userDefaultToClean.forEach {
                UserDefaults.standard.removeObject(forKey: $0)
            }
            UserDefaults.standard.synchronize()
        }
    }
}

extension AppDelegate: WalletChangesObserver {

    func walletCertificatesDidUpdate() {}
    func walletFavoriteCertificateDidUpdate() {
        updateAppShortcut()
    }
    func walletSmartStateDidUpdate() {}
    func walletBlacklistDidUpdate() {}
}
