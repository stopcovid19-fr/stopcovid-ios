// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
//
//  KeyFigureComparison.swift
//  TousAntiCovid
//
//  Created by Lunabee Studio / Date - 18/01/2023 - for the TousAntiCovid project.
//

import UIKit

struct KeyFigureComparison {
    let comparisonDescription: String?

    let comparisonLastWeekTitle: String
    let comparisonLastWeekValue: String?
    let comparisonLastWeekValueColor: UIColor
    let comparisonLastWeekDate: String?

    let comparisonMaxTitle: String
    let comparisonMaxValue: String?
    let comparisonMaxValueColor: UIColor
    let comparisonMaxDate: String?

    let comparisonMinTitle: String
    let comparisonMinValue: String?
    let comparisonMinValueColor: UIColor
    let comparisonMinDate: String?
}
