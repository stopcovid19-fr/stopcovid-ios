// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
//
//  WidgetDCCManager.swift
//  TousAntiCovid
//
//  Created by Lunabee Studio / Date - 22/07/2021 - for the TousAntiCovid project.
//

import Foundation
import WidgetKit
#if !WIDGET
import UIKit
import PKHUD
#endif

@available(iOS 14.0, *)
final class WidgetDCCManager {

    static let shared: WidgetDCCManager = .init()
    private init() {}

    static let scheme: String = "tousanticovid"

    @WidgetDCCUserDefault(key: .bottomText)
    private(set) var bottomText: String = ""

    @WidgetDCCUserDefault(key: .noCertificateText)
    private(set) var noCertificateText: String = ""

    @WidgetDCCUserDefault(key: .certificateQrCodeData)
    private(set) var certificateQrCodeData: Data?

#if !WIDGET
    @WidgetDCCUserDefault(key: .isOnboardingDone)
    var isOnboardingDone: Bool = false
    private var currentCertificate: EuropeanCertificate?

    func processUserActivity(_ userActivity: NSUserActivity) {
        guard userActivity.activityType == "fr.gouv.stopcovid.ios.Widget.dcc" && isOnboardingDone else { return }
        NotificationCenter.default.post(name: WalletManager.shared.favoriteDccId.isNil ? .openWallet : .openCertificateQRCode, object: nil)
    }

    func start() {
        addObservers()
        updateCertificate()
        reloadData()
    }

    private func addObservers() {
        LocalizationsManager.shared.addObserver(self)
        WalletManager.shared.addObserver(self)
        NotificationCenter.default.addObserver(self, selector: #selector(appDidBecomeActive), name: UIApplication.didBecomeActiveNotification, object: nil)
    }
    
    private func updateCertificate() {
        if let certificate = WalletManager.shared.favoriteCertificate as? EuropeanCertificate {
            currentCertificate = certificate
            certificateQrCodeData = certificate.codeImage?.jpegData(compressionQuality: 1.0)
        } else {
            currentCertificate = nil
            certificateQrCodeData = nil
        }
    }
    
    @objc private func appDidBecomeActive() {
        updateCertificate()
        reloadData()
    }
#endif

}

#if !WIDGET
@available(iOS 14.0, *)
extension WidgetDCCManager: LocalizationsChangesObserver {

    func localizationsChanged() {
        reloadData()
    }

    func reloadData() {
        bottomText = "widget.dcc.full".localized
        noCertificateText = "widget.dcc.empty".localized
        WidgetCenter.shared.reloadAllTimelines()
    }

}

@available(iOS 14.0, *)
extension WidgetDCCManager: WalletChangesObserver {

    func walletCertificatesDidUpdate() {}
    func walletFavoriteCertificateDidUpdate() {
        updateCertificate()
        reloadData()
    }
    func walletSmartStateDidUpdate() {}
    func walletBlacklistDidUpdate() {}

}
#endif
