// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
//
//  FeaturedInfoConstant.swift
//  TousAntiCovid
//
//  Created by Lunabee Studio / Date - 05/01/2023 - for the TousAntiCovid project.
//

enum FeaturedInfoConstant {
    static let baseUrl: String = "https://\(Constant.Server.staticResourcesRootDomain)/json/version-\(Constant.Server.jsonVersion)/InfoCenter"
    

}
