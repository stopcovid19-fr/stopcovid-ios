// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
//
//  BlacklistManaging.swift
//  TousAntiCovid
//
//  Created by Lunabee Studio / Date - 12/10/2022 - for the TousAntiCovid project.
//

import StorageSDK

protocol BlacklistManaging {
    func start(storageManager: StorageManager)
    func clearAllData()
    func isBlacklisted(certificateHash: String, in hashesList: [String]?) -> Bool
    func isCertificateBlacklisted(_ certificate: WalletCertificate, queue: DispatchQueue, completion: @escaping (_ isBlacklisted: Bool?) -> ())
    func blacklistedCertificates(in certificates: [WalletCertificate], forceReload: Bool, queue: DispatchQueue, completion: @escaping (_ success: Bool, _ blacklistedCertificates: [WalletCertificate]?) -> ())
}
