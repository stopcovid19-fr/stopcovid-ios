// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
//
//  EUTagsManager.swift
//  TousAntiCovid
//
//  Created by Lunabee Studio / Date - 04/08/2022 - for the TousAntiCovid project.
//

import UIKit
import ServerSDK

final class EUTagsManager {
    
    static let shared: EUTagsManager = .init()
    private init() {}
    
    var tags: [String] = []

    func start() {
        writeInitialFileIfNeeded()
        loadLocalTags()
        addObserver()
    }
    
    func isEuTag(for certificateManufacturer: String) -> Bool {
        tags.contains(certificateManufacturer)
    }
}

// MARK: - App state observer -
private extension EUTagsManager {
    func addObserver() {
        NotificationCenter.default.addObserver(self, selector: #selector(appDidBecomeActive), name: UIApplication.didBecomeActiveNotification, object: nil)
    }
    
    @objc func appDidBecomeActive() {
        fetchTagsFile()
    }
}

// MARK: - All fetching methods -
private extension EUTagsManager {
     func fetchTagsFile(_ completion: (() -> ())? = nil) {
        let dataTask: URLSessionDataTask = UrlSessionManager.shared.session.dataTaskWithETag(with: Constant.Server.euTagsUrl) { data, response, error, updateEtag in
            guard let data = data else {
                DispatchQueue.main.async {
                    completion?()
                }
                return
            }
            do {
                guard let tags = try JSONSerialization.jsonObject(with: data, options: []) as? [String], !tags.isEmpty else {
                    DispatchQueue.main.async { completion?() }
                    return
                }
                self.tags = tags
                try data.write(to: self.localTagsUrl())
                updateEtag()
                DispatchQueue.main.async { completion?() }
            } catch {
                DispatchQueue.main.async { completion?() }
            }
        }
        dataTask.resume()
    }
}

// MARK: - Local files management -
private extension EUTagsManager {
    func localTagsUrl() -> URL {
        let directoryUrl: URL = self.createWorkingDirectoryIfNeeded()
        return directoryUrl.appendingPathComponent("tags-ue-list-codes.json")
    }

    func loadLocalTags() {
        let localUrl: URL = localTagsUrl()
        guard FileManager.default.fileExists(atPath: localUrl.path) else { return }
        guard let data = try? Data(contentsOf: localUrl) else { return }
        tags = (try? JSONSerialization.jsonObject(with: data, options: []) as? [String]) ?? []
    }

    func writeInitialFileIfNeeded() {
        let fileUrl: URL = Bundle.main.url(forResource: Constant.Server.euTagsUrl.lastPathComponent, withExtension: nil)!
        let destinationFileUrl: URL = localTagsUrl()
        if !FileManager.default.fileExists(atPath: destinationFileUrl.path) {
            try? FileManager.default.removeItem(at: destinationFileUrl)
            try? FileManager.default.copyItem(at: fileUrl, to: destinationFileUrl)
        }
    }

    func createWorkingDirectoryIfNeeded() -> URL {
        let directoryUrl: URL = FileManager.libraryDirectory().appendingPathComponent("EUTags")
        if !FileManager.default.fileExists(atPath: directoryUrl.path, isDirectory: nil) {
            try? FileManager.default.createDirectory(at: directoryUrl, withIntermediateDirectories: false, attributes: nil)
        }
        return directoryUrl
    }
}
