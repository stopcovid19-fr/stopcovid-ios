// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
//
//  WalletValidityRulesManager.swift
//  TousAntiCovid
//
//  Created by Lunabee Studio / Date - 03/02/2022 - for the TousAntiCovid project.
//

import Foundation
import ServerSDK

final class ValidityRulesObserverWrapper: NSObject, WalletRulesObserverWrapper {
    weak var observer: WalletRulesObserver?
}

private struct ValidityData: Comparable {
    var startDelayInSec: Double?
    var endDelayInSec: Double?
    var ageMinExpSec: Double
    var pivotDate: Date
    
    static func < (lhs: ValidityData, rhs: ValidityData) -> Bool {
        lhs.endDelayInSec ?? .greatestFiniteMagnitude < rhs.endDelayInSec ?? .greatestFiniteMagnitude
    }
}

struct ValidityInfo {
    var startTimestamp: Double?
    var endTimestamp: Double?
}

final class WalletValidityRulesManager: WalletRulesSyncManager {
    
    typealias DecodableJSONObject = WalletRule
    typealias ObserverWrapperObject = ValidityRulesObserverWrapper
    
    static let shared: WalletValidityRulesManager = .init()
    private init() {}
    
    var observers: [ValidityRulesObserverWrapper] = []
    var objects: [WalletRule] = []
    var ruleType: WalletRulesConstant.WalletRulesType = .validity
    var workingDirectoryUrl: URL { WalletRulesConstant.workingDirectoryUrl }
        
    @UserDefault(key: .lastInitialExpRulesBuildNumber)
    private var lastInitialExpRulesBuildNumber: String? = nil
    
    @UserDefault(key: .lastExpRulesUpdateDate)
    private var lastExpRulesUpdateDate: Date = .distantPast
    
    func canUpdateData() -> Bool { Date().timeIntervalSince1970 - lastExpRulesUpdateDate.timeIntervalSince1970 >= WalletRulesConstant.minDurationBetweenUpdatesInSeconds }
    
    func saveUpdatedAt() { lastExpRulesUpdateDate = Date() }
    
    func lastBuildNumber() -> String? { lastInitialExpRulesBuildNumber }
    
    func saveLastBuildNumber(_ buildNumber: String) { lastInitialExpRulesBuildNumber = buildNumber }
    
    func start() {
        start(with: ruleType.remoteUrl, bundle: Bundle.main, forceWrite: false)
    }
    
    func validity(now: Date,
                  certificateType: WalletConstant.CertificateType,
                  certificateAlignedTimestamp: Double,
                  certificateAlignedDateOfBirth: Date,
                  certificateMedicalProductCode: String?,
                  certificateCurrentDosesNumber: Int?,
                  certificateTargetDosesNumber: Int?,
                  certificatePrefix: String?,
                  certificateIsTestNegative: Bool?,
                  vaccinsConfig: Vaccins?) -> ValidityInfo? {
        let validityDatas: [ValidityData] = objects.compactMap { rule in
            validityFor(rule: rule,
                        now: now,
                        certificateType: certificateType,
                        certificateAlignedTimestamp: certificateAlignedTimestamp,
                        certificateAlignedDateOfBirth: certificateAlignedDateOfBirth,
                        certificateMedicalProductCode: certificateMedicalProductCode,
                        certificateCurrentDosesNumber: certificateCurrentDosesNumber,
                        certificateTargetDosesNumber: certificateTargetDosesNumber,
                        certificatePrefix: certificatePrefix,
                        certificateIsTestNegative: certificateIsTestNegative,
                        vaccinsConfig: vaccinsConfig)
        }

        let relevantValidity: ValidityData? = validityDatas.sorted(by: { $0.pivotDate < $1.pivotDate }).enumerated().first { index, validity in
            guard index < validityDatas.count - 1 else { return true }
            guard let endDelayInSec = validity.endDelayInSec else { return false }
            return certificateAlignedTimestamp + endDelayInSec < validityDatas[index + 1].pivotDate.timeIntervalSince1970
        }?.element
        
        guard let relevantValidity = relevantValidity else { return nil }
        
        var exp: Double? = nil
        var start: Double? = nil
        if let endInSec = relevantValidity.endDelayInSec {
            let age: Int = certificateAlignedDateOfBirth.age(at: now)
            exp = max(certificateAlignedDateOfBirth.birthdayTimestamp(for: age) + relevantValidity.ageMinExpSec,
                                  max(certificateAlignedTimestamp + endInSec, relevantValidity.pivotDate.timeIntervalSince1970))
        }
        if let startInSec = relevantValidity.startDelayInSec {
            start = certificateAlignedTimestamp + startInSec
        }
        return ValidityInfo(startTimestamp: start, endTimestamp: exp)
    }
}

// MARK: - Utils
private extension WalletValidityRulesManager {
    func validityFor(rule: WalletRule?,
                     now: Date,
                     certificateType: WalletConstant.CertificateType,
                     certificateAlignedTimestamp: Double,
                     certificateAlignedDateOfBirth: Date,
                     certificateMedicalProductCode: String?,
                     certificateCurrentDosesNumber: Int?,
                     certificateTargetDosesNumber: Int?,
                     certificatePrefix: String?,
                     certificateIsTestNegative: Bool?,
                     vaccinsConfig: Vaccins?) -> ValidityData? {
        guard let walletRule = rule else { return nil }
        let age: Int = certificateAlignedDateOfBirth.age(at: now)
        guard let ruleForAge: RuleForAge = walletRule.rule(for: age) else { return nil }
        // If pivot is in future
        if walletRule.pivotDate > now {
            // Check if userAge at this pivot makes him elligible to the rules
            guard certificateAlignedDateOfBirth.age(at: walletRule.pivotDate) >= ruleForAge.age else { return nil }
        } else {
            guard age >= ruleForAge.age else { return nil }
        }
        let conditions: [RuleCondition]? = ruleForAge.conditions(certificateType: certificateType,
                                                                 certificateIsTestNegative: certificateIsTestNegative)
        guard let tempValidity = conditions?.firstVerified(certificateType: certificateType,
                                                           certificateMedicalProductCode: certificateMedicalProductCode,
                                                           certificateCurrentDosesNumber: certificateCurrentDosesNumber,
                                                           certificateTargetDosesNumber: certificateTargetDosesNumber,
                                                           certificatePrefix: certificatePrefix,
                                                           certificateIsTestNegative: certificateIsTestNegative,
                                                           vaccinsConfig: vaccinsConfig)?.validity else { return nil }
        return ValidityData(startDelayInSec: tempValidity.startInSec, endDelayInSec: tempValidity.endInSec, ageMinExpSec: ruleForAge.ageMinExpSec, pivotDate: walletRule.pivotDate)
    }
}

extension Date {
    func birthdayTimestamp(for age: Int) -> Double {
        Calendar.utc.date(byAdding: .year, value: age, to: self)?.timeIntervalSince1970 ?? 0
    }
    
    func age(at date: Date) -> Int {
        Calendar.utc.dateComponents([.year], from: self, to: date.roundingToMidnightPastOne()).year ?? 0
    }
}
