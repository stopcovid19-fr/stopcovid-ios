// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
//
//  WalletManager.swift
//  TousAntiCovid
//
//  Created by Lunabee Studio / Date - 17/03/2021 - for the TousAntiCovid project.
//

import UIKit
import StorageSDK
import ServerSDK

protocol WalletChangesObserver: AnyObject {
    
    func walletCertificatesDidUpdate()
    func walletFavoriteCertificateDidUpdate()
    func walletSmartStateDidUpdate()
    func walletBlacklistDidUpdate()

}

final class WalletObserverWrapper: NSObject {
    
    weak var observer: WalletChangesObserver?
    
    init(observer: WalletChangesObserver) {
        self.observer = observer
    }

}

final class WalletManager {

    static let shared: WalletManager = .init()
    private init() {}
    
    private let queue: DispatchQueue = .init(label: "blacklist-v4-queue", qos: .userInitiated)
    
    @UserDefault(key: .blacklistIsDirty)
    private var blacklistIsDirty: Bool = false
    
    var walletCertificates: [WalletCertificate] {
        get {
            let dbCertificatesCount: Int = storageManager.walletCertificates().count
            if _walletCertificates.count != dbCertificatesCount { reloadCertificates() }

            if _walletCertificates.isEmpty && dbCertificatesCount > 0 {
                StackLogger.log(symbols: Thread.callStackSymbolsString, message: "Displaying 0 certificates but having \(dbCertificatesCount) certificates in db.")
            }

            return _walletCertificates
        }
        set { _walletCertificates = newValue }
    }
    
    var blacklistedCertificates: [WalletCertificate] = []
    
    var lastRelevantCertificates: [EuropeanCertificate]? {
        didSet {
            // WalletState could have changed
            notifyWalletSmartState()
        }
    }
    var areThereCertificatesToLoad: Bool { _walletCertificates.count != storageManager.walletCertificates().count }

    // MARK: Sections
    enum WalletCertificateSection: CaseIterable {
        case favorite, valid, validSoon, negativeTest, expired, other
    }

    var sections: [WalletCertificateSection: [WalletCertificate]] {
        var sections: [WalletCertificateSection: [WalletCertificate]] = [:]
        WalletCertificateSection.allCases.forEach { sections[$0] = [] }
        walletCertificates.forEach { certificate in
            /// Favorite section
            if certificate.id == favoriteCertificate?.id {
                sections[.favorite]?.append(certificate)
            } else if let eCertificate = certificate as? EuropeanCertificate {
                /// Valid soon european certificate
                if (validity(eCertificate)?.startTimestamp).map({ $0 > Date().timeIntervalSince1970 }) == true {
                    sections[.validSoon]?.append(eCertificate)
                /// Valid european test certificate
                } else if eCertificate.isValidNegativeTest == true {
                    sections[.negativeTest]?.append(eCertificate)
                /// Expired european certificate
                } else if isPassExpired(for: eCertificate) || eCertificate.isExpired {
                    sections[.expired]?.append(eCertificate)
                /// Valid european certificate
                } else if validity(eCertificate)?.startTimestamp.map({ $0 <= Date().timeIntervalSince1970 }) == true && (validity(eCertificate)?.endTimestamp ?? .greatestFiniteMagnitude) > Date().timeIntervalSince1970 {
                    sections[.valid]?.append(eCertificate)
                } else {
                    /// Other european certificate
                    sections[.other]?.append(eCertificate)
                }
            /// Other certificate
            } else {
                sections[.other]?.append(certificate)
            }
        }
        return sections
    }

    var areThereCertificatesNeedingAttention: Bool {
        !walletCertificates.first {
            if let europeCertificate = $0 as? EuropeanCertificate {
                return (europeCertificate.isNegativeTest == false && europeCertificate.type == .sanitaryEurope) || isCertificateBlacklisted(europeCertificate)
            } else if $0.is2dDoc {
                return isCertificateBlacklisted($0)
            } else {
                return false
            }
        }.isNil
    }

    var isWalletActivated: Bool {
        return ParametersManager.shared.displaySanitaryCertificatesWallet
        
    }
    
    var isMultiPassActivated: Bool {
        return ParametersManager.shared.isMultiPassActivated
        
    }
    
    @UserDefault(key: .smartWalletActivated)
    var smartWalletActivated: Bool = true {
        didSet {
            if smartWalletActivated {
                updateLastRelevantCertificates()
            } else {
                lastRelevantCertificates = nil
            }
        }
    }
    
    // Notifications
    @UserDefault(key: .smartWalletSentNotificationsIds)
    var smartWalletSentNotificationsIds: [String] = []
    @UserDefault(key: .smartWalletEligibilitySentNotifications)
    var smartWalletEligibilitySentNotifications: [String: Double] = [:]
    @UserDefault(key: .smartWalletLastNotificationTimestamp)
    var smartWalletLastNotificationTimestamp: Double = Date.distantPast.timeIntervalSince1970
    @UserDefault(key: .smartWalletLastNotificationCalculationTimestamp)
    var smartWalletLastNotificationCalculationTimestamp: Double = Date.distantPast.timeIntervalSince1970

    var favoriteCertificate: WalletCertificate? { _walletCertificates.filter { $0.id == favoriteDccId }.first ?? loadCertificate(for: favoriteDccId) }

    @UserDefault(key: .favoriteDccId)
    private(set) var favoriteDccId: String? {
        didSet { notifyFavoriteCertificate() }
    }
    
    private var _walletCertificates: [WalletCertificate] = []
    private var europeanCertificates: [EuropeanCertificate] { walletCertificates.compactMap { $0 as? EuropeanCertificate } }
    private var observers: [WalletObserverWrapper] = []
    private var storageManager: StorageManager!
    
    func start(storageManager: StorageManager) {
        self.storageManager = storageManager
        addObservers()
        CoreManager.publicKeyEncoder = self
        reloadCertificates()
    }
    
    func setFavorite(certificate: WalletCertificate) {
        favoriteDccId = certificate.id
    }
    
    func removeFavorite() {
        favoriteDccId = nil
    }
    
    func isCertificateEuValidated(_ certificate: EuropeanCertificate) -> Bool {
        guard certificate.isAntigenTest else { return true }
        guard let manufacturer = certificate.hCert.testStatements.first?.manufacturer else { return true }
        return EUTagsManager.shared.isEuTag(for: manufacturer)
    }
    
    func isCertificateBlacklisted(_ certificate: WalletCertificate) -> Bool {
        blacklistedCertificates.first { $0.uniqueHash == certificate.uniqueHash } != nil
    }
    
    func saveCertificate(_ certificate: WalletCertificate) {
        StackLogger.log(symbols: Thread.callStackSymbolsString, message: "Saving a certificate.")
        storageManager.saveWalletCertificate(certificate.toRawCertificate())
        updateBlacklistWith(new: certificate, queue: queue)
    }

    func deleteCertificate(id: String) {
        StackLogger.log(symbols: Thread.callStackSymbolsString, message: "Deleting a certificate.")
        storageManager.deleteWalletCertificate(id: id)
        if favoriteDccId == id { favoriteDccId = nil }
        NotificationsManager.shared.cancelCompletedVaccinationNotification(for: id)
    }
    
    func clearAllData() {
        StackLogger.log(symbols: Thread.callStackSymbolsString, message: "Deleting all certificates.")
        NotificationsManager.shared.cancelCompletedVaccinationNotifications(for: europeanCertificates.map { $0.id })
        storageManager.deleteWalletCertificates()
        favoriteDccId = nil
        BlacklistManager.current.clearAllData()
    }

    func isDuplicatedCertificate(_ certificate: WalletCertificate)  -> Bool {
        walletCertificates.first { $0.uniqueHash == certificate.uniqueHash } != nil
    }
    func updateLastRelevantCertificates() {
        lastRelevantCertificates = getLastRelevantCertificates()
    }


    private func loadCertificate(for id: String?) -> WalletCertificate? {
        guard let certificate = storageManager.walletCertificates().filter({ $0.id == id }).first.flatMap({ from(rawCertificate: $0) }) else { return nil }
        _walletCertificates.append(certificate)
        return certificate
    }

    private func reloadCertificates(forceReloadFor forcedIds: [String] = []) {
        let loadedCertificatesDict: [String: [WalletCertificate]] = [String: [WalletCertificate]](grouping: _walletCertificates) { $0.id }
        _walletCertificates = storageManager.walletCertificates().compactMap {
            if forcedIds.contains($0.id) {
                return from(rawCertificate: $0)
            } else {
                return loadedCertificatesDict[$0.id]?.first ?? from(rawCertificate: $0)
            }
        }
        updateLastRelevantCertificates()
    }

    private func addObservers() {
        LocalizationsManager.shared.addObserver(self)
        NotificationCenter.default.addObserver(self, selector: #selector(walletCertificateDataDidUpdate), name: .walletCertificateDataDidChange, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(appDidBecomeActive), name: UIApplication.didBecomeActiveNotification, object: nil)
    }

    @objc private func walletCertificateDataDidUpdate() {
        reloadCertificates()
        notify()
    }

    @objc private func appDidBecomeActive() {
        updateWalletBlacklistedCertificates(queue: queue)
    }
    
    private func checkCertificateSignature(_ certificate: WalletCertificate) -> Bool {
        guard let publicKey = certificate.publicKey?.cleaningPEMStrings() else { return false }
        guard let publicKeyData = Data(base64Encoded: publicKey) else { return false }
        guard let messageData = certificate.message else { return false }
        guard let rawSignatureData = certificate.signature else { return false }
        do {
            let publicSecKey: SecKey = try SecKey.publicKeyfromDerData(publicKeyData)
            let algorithm: SecKeyAlgorithm = .ecdsaSignatureMessageX962SHA256
            let canVerify: Bool = SecKeyIsAlgorithmSupported(publicSecKey, .verify, algorithm)
            guard canVerify else { return false }
            
            let encodedSignatureData: Data = try rawSignatureData.derEncodedSignature()
            
            var error: Unmanaged<CFError>?
            let isSignatureValid: Bool = SecKeyVerifySignature(publicSecKey,
                                                               algorithm,
                                                               messageData as CFData,
                                                               encodedSignatureData as CFData,
                                                               &error)
            return isSignatureValid
        } catch {
            print(error)
            return false
        }
    }

}

extension WalletManager {
    
    func getWalletCertificate(from url: URL) throws -> WalletCertificate {
        var certificate: WalletCertificate
        switch url.path {
        case WalletConstant.URLPath.wallet.rawValue,
             WalletConstant.URLPath.wallet2D.rawValue:
            certificate = try extractCertificateFrom(url: url)
        case WalletConstant.URLPath.walletDCC.rawValue:
            if DeepLinkingManager.shared.isComboDeeplink(url),
               let certificatStringUrl = url.absoluteString.components(separatedBy: WalletConstant.Separator.declareCode.rawValue).first,
               let certificatUrl = URL(string: certificatStringUrl) {
                certificate = try extractEuropeanCertificateFrom(url: certificatUrl)
            } else {
                certificate = try extractEuropeanCertificateFrom(url: url)
            }
        default:
            throw WalletError.parsing.error
        }
        return certificate
    }
}

extension WalletManager {
    
    static func certificateType(doc: String) -> WalletConstant.CertificateType? {
        [WalletConstant.CertificateType.sanitary, WalletConstant.CertificateType.vaccination].first { doc ~> $0.validationRegex }
    }
    
    static func certificateType(hCert: HCert) -> WalletConstant.CertificateType {
        switch hCert.certificateType {
        case .test:
            return .sanitaryEurope
        case .vaccine:
            return .vaccinationEurope
        case .recovery:
            return .recoveryEurope
        case .exemption:
            return .exemptionEurope
        case .unknown:
            return .unknown
        }
    }
    
    static func certificateTypeFromHeaderInUrl(_ url: URL) -> WalletConstant.CertificateType? {
        guard let urlComponents = URLComponents(url: url, resolvingAgainstBaseURL: false) else { return nil }
        guard let completeMessage = urlComponents.queryItems?.first(where: { $0.name == "v" })?.value ?? urlComponents.fragment else { return nil }
        switch urlComponents.path {
        case WalletConstant.URLPath.wallet.rawValue, WalletConstant.URLPath.wallet2D.rawValue:
            return WalletConstant.CertificateType.allCases.first { completeMessage ~> $0.headerDetectionRegex }
        case WalletConstant.URLPath.walletDCC.rawValue:
            guard let hCert = try? HCert(from: completeMessage) else { return nil }
            return certificateType(hCert: hCert)
        default:
            return nil
        }
    }

}

// MARK: Get certificate from RawWalletCertificate
extension WalletManager {

    private func from(rawCertificate: RawWalletCertificate) -> WalletCertificate? {
        if let certificateType = WalletManager.certificateType(doc: rawCertificate.value) {
            switch certificateType {
            case .sanitary:
                return SanitaryCertificate(id: rawCertificate.id, value: rawCertificate.value, type: certificateType)
            case .vaccination:
                return VaccinationCertificate(id: rawCertificate.id, value: rawCertificate.value, type: certificateType)
            default:
                return nil
            }
        } else if let hCert = try? HCert(from: rawCertificate.value) {
            return EuropeanCertificate(id: rawCertificate.id,
                                       value: rawCertificate.value,
                                       type: WalletManager.certificateType(hCert: hCert),
                                       hCert: hCert)
        } else {
            return nil
        }
    }

    private func from(doc: String) -> WalletCertificate? {
        guard let certificateType = WalletManager.certificateType(doc: doc) else { return nil }
        switch certificateType {
        case .sanitary:
            return SanitaryCertificate(value: doc, type: certificateType)
        case .vaccination:
            return VaccinationCertificate(id: UUID().uuidString, value: doc, type: certificateType)
        default:
            return nil
        }
    }
}

extension WalletManager {

    @discardableResult
    func extractCertificateFrom(url: URL) throws -> WalletCertificate {
        guard let urlComponents = URLComponents(url: url, resolvingAgainstBaseURL: false) else { throw WalletError.parsing.error }
        guard let completeMessage = urlComponents.queryItems?.first(where: { $0.name == "v" })?.value ?? urlComponents.fragment else { throw WalletError.parsing.error }
        return try extractCertificateFrom(doc: completeMessage)
    }

    func extractCertificateFrom(doc: String) throws -> WalletCertificate {
        guard let certificate = from(doc: doc) else { throw WalletError.parsing.error }
        guard checkCertificateSignature(certificate) else { throw WalletError.signature.error }
        return certificate
    }

    @discardableResult
    func extractEuropeanCertificateFrom(url: URL) throws -> WalletCertificate {
        guard let urlComponents = URLComponents(url: url, resolvingAgainstBaseURL: false) else { throw WalletError.parsing.error }
        guard let completeMessage = urlComponents.fragment else { throw WalletError.parsing.error }
        return try extractEuropeanCertificateFrom(doc: completeMessage)
    }

    func extractEuropeanCertificateFrom(id: String = UUID().uuidString, doc: String) throws -> EuropeanCertificate {
        let hCert: HCert
        do {
            hCert = try HCert(from: doc)
        } catch {
            throw WalletError.parsing.error
        }
        let certificateType: WalletConstant.CertificateType = WalletManager.certificateType(hCert: hCert)
        guard certificateType != .unknown else {
            throw WalletError.parsing.error
        }
        let certificate: EuropeanCertificate = EuropeanCertificate(id: id, value: doc, type: certificateType, hCert: hCert)
        guard certificate.isForeignCertificate || hCert.cryptographicallyValid else { throw WalletError.signature.error }
        return certificate
    }

}

extension WalletManager: PublicKeyStorageDelegate {

    func getEncodedPublicKeys(for kidStr: String) -> [String] {
        DccCertificatesManager.shared.certificates(for: kidStr).map {
            if $0.hasPrefix("-----BEGIN PUBLIC KEY-----") {
                return $0
            } else {
                return "-----BEGIN PUBLIC KEY-----" + $0 + "-----END PUBLIC KEY-----"
            }
        }
    }

}

// MARK: - MultiPass -
extension WalletManager {

    func generateMultiPassDccFrom(certificates: [WalletCertificate], completion: ((_ result: Result<WalletCertificate, Error>) -> ())?) {
        do {
            guard let remotePublicKey = ParametersManager.shared.activityPassGenerationServerPublicKey else {
                completion?(.failure(NSError.localizedError(message: "Server public key not found", code: 0)))
                return
            }
            guard let remotePublicKeyData = Data(base64Encoded: remotePublicKey) else {
                completion?(.failure(NSError.localizedError(message: "Server public key not properly formatted", code: 0)))
                return
            }
            let keyPair: CryptoKeyPair = try Crypto.generateKeys()
            let sharedSecret: Data = try Crypto.generateSecret(localPrivateKey: keyPair.privateKey, remotePublicKey: remotePublicKeyData)
            let encryptionKey: Data = try Crypto.generateConversionEncryptionKey(sharedSecret: sharedSecret)
            let encodedCertificates: [String] = try certificates.map { try Crypto.encrypt($0.value, key: encryptionKey).base64EncodedString() }
            MultiPassServer.shared.generateMultiPassDcc(encodedCertificates: encodedCertificates, publicKey: keyPair.publicKeyData.base64EncodedString()) { result in
                switch result {
                case let .success(base64EncryptedResponse):
                    do {
                        let responseJson: String = try Crypto.decrypt(base64EncryptedResponse, key: encryptionKey)
                        guard let responseData = responseJson.data(using: .utf8) else {
                            completion?(.failure(NSError.localizedError(message: "Unable to parse server response", code: 0)))
                            return
                        }
                        let response: MultiPassAggregateResponseContent = try JSONDecoder().decode(MultiPassAggregateResponseContent.self, from: responseData)
                        let certificate: WalletCertificate = try self.extractEuropeanCertificateFrom(doc: response.certificate)
                        let rawCertificate: RawWalletCertificate = RawWalletCertificate(value: response.certificate, expiryDate: nil)
                        self.storageManager.saveWalletCertificate(rawCertificate)
                        self.reloadCertificates()
                        self.notify()
                        completion?(.success(certificate))
                    } catch {
                        completion?(.failure(error))
                    }
                case let .failure(error):
                    completion?(.failure(error))
                }
            }
        } catch {
            completion?(.failure(error))
        }
    }
}

// MARK: - Smart wallet
extension WalletManager {
    func getLastRelevantCertificates() -> [EuropeanCertificate]? {
        getSmartWalletProfiles().compactMap { relevantCertificate(in: $0.value) }
    }
    
    func getSmartWalletProfiles() -> [String: [EuropeanCertificate]] {
        let europeanCertificates: [EuropeanCertificate] = _walletCertificates.filter { ($0 as? EuropeanCertificate)?.hasLunarBirthdate == false } as? [EuropeanCertificate] ?? []
        return Dictionary(grouping: europeanCertificates) { $0.smartWalletProfileId }
    }
    
    func relevantCertificate(in certificates: [EuropeanCertificate], today: Date = Date()) -> EuropeanCertificate? {
        let dccFiltered: [EuropeanCertificate] = certificates.filter { ($0.isLastDose == true || $0.type == .recoveryEurope || $0.isNegativeTest == false) && !$0.isExpired }
        let sortedDccVaxComplete: [EuropeanCertificate] = certificates.filter { $0.isLastDose == true && !$0.isExpired }
            .sorted(by: { $0.alignedTimestamp > $1.alignedTimestamp })
        
        if dccFiltered.contains(where: { expiryTimestamp($0, today: today).isNil && eligibility($0, today: today)?.eligibilityTimestamp == nil }) { return nil }
        if eligibility(sortedDccVaxComplete.first, today: today)?.eligibilityTimestamp == nil { return nil }
        return dccFiltered.sorted(by: { $0.alignedTimestamp > $1.alignedTimestamp })
            .first
    }
}

// MARK: - Multipass
extension WalletManager {
    private struct SimilarProfile: Hashable {
        var certificate: EuropeanCertificate
        var shouldShowBirthdate: Bool
        
        static func == (lhs: SimilarProfile, rhs: SimilarProfile) -> Bool {
            lhs.certificate == rhs.certificate
        }
        
        func hash(into hasher: inout Hasher) {
            hasher.combine(certificate.value)
        }
    }
    
    private func walletLunarEuropeanCertificates() -> [EuropeanCertificate] {
        _walletCertificates.filter { ($0 as? EuropeanCertificate)?.hasLunarBirthdate == false } as? [EuropeanCertificate] ?? []
    }
    
    private func getMultiPassProfiles(in certificates: [EuropeanCertificate]) -> [String: [EuropeanCertificate]] {
        let europeanCertificates: [EuropeanCertificate] = certificates
        return Dictionary(grouping: europeanCertificates) { $0.multiPassProfileId }
    }
    
    func getWalletMultiPassProfiles() -> [String: [EuropeanCertificate]] {
        getMultiPassProfiles(in: walletLunarEuropeanCertificates())
    }

    func getSimilarProfilesForMultiPass(in europeanCertificates: [EuropeanCertificate]) -> Set<EuropeanCertificate> {
        var similarLastnameSet: Set<EuropeanCertificate> = .init()
        var similarFirstnameSet: Set<EuropeanCertificate> = .init()
        var similarDateOfBirthSet: Set<EuropeanCertificate> = .init()
        europeanCertificates.forEach { cert in
            europeanCertificates.forEach { otherCert in
                if otherCert !== cert && cert.isProfileMismatchForLastname(to: otherCert) {
                    similarLastnameSet.insert(otherCert)
                }
                if otherCert !== cert && cert.isProfileMismatchForFirstname(to: otherCert) {
                    similarFirstnameSet.insert(otherCert)
                }
                if otherCert !== cert && cert.isProfileMismatchForDateOfBirth(to: otherCert) {
                    similarDateOfBirthSet.insert(otherCert)
                }
            }
        }
        return similarLastnameSet.union(similarFirstnameSet).union(similarDateOfBirthSet)
    }
    
    func hasCertificateSimilarProfileInWallet(_ certificate: EuropeanCertificate) -> Bool {
        var walletEuropeanCertificates: [EuropeanCertificate] = walletLunarEuropeanCertificates()
        walletEuropeanCertificates.append(certificate)
        let walletMultiPassCertificates: [EuropeanCertificate] = Array(getMultiPassProfiles(in: walletEuropeanCertificates).compactMap { $0.value.first })
        return getSimilarProfilesForMultiPass(in: walletMultiPassCertificates).compactMap { $0.multiPassProfileId }.contains(certificate.multiPassProfileId)
    }
    
    func getSimilarProfileNames() -> [String] {
        let europeanCertificates: [EuropeanCertificate] = getWalletMultiPassProfiles().compactMap { $0.value.first }
        return getSimilarProfilesForMultiPass(in: europeanCertificates).compactMap { "\($0.fullName) (\(Date(timeIntervalSince1970: $0.birthdate).shortDateFormatted(timeZoneIndependant: true)))" }.sorted(by: <)
    }

    func relevantCertificateForMultiPass(in certificates: [EuropeanCertificate]) -> [EuropeanCertificate] {
        let nowTimestamp: Double = Date().timeIntervalSince1970
        let filteredCertificates: [EuropeanCertificate] = certificates.filter { certificate in
            let isCertificateAllowed: Bool = !isCertificateBlacklisted(certificate)
            let isCertificateStillValid: Bool = certificate.timestamp + Double(ParametersManager.shared.multiPassConfiguration.testMaxHours) * 3600.0 >= nowTimestamp
            let isMultipass: Bool = certificate.isFromMultiPassIssuer || certificate.type == .multiPass
            return (certificate.isNegativeTest == true ? isCertificateStillValid && isCertificateAllowed : isCertificateAllowed) && !isMultipass
        }
        return Array(Set<EuropeanCertificate>(filteredCertificates))
    }
}

extension WalletManager {

    func addObserver(_ observer: WalletChangesObserver) {
        guard observerWrapper(for: observer) == nil else { return }
        observers.append(WalletObserverWrapper(observer: observer))
    }

    func removeObserver(_ observer: WalletChangesObserver) {
        guard let wrapper = observerWrapper(for: observer), let index = observers.firstIndex(of: wrapper) else { return }
        observers.remove(at: index)
    }

    private func observerWrapper(for observer: WalletChangesObserver) -> WalletObserverWrapper? {
        observers.first { $0.observer === observer }
    }

    private func notify() {
        observers.forEach { $0.observer?.walletCertificatesDidUpdate() }
    }
    
    private func notifyFavoriteCertificate() {
        observers.forEach { $0.observer?.walletFavoriteCertificateDidUpdate() }
    }

    private func notifyWalletSmartState() {
        observers.forEach { $0.observer?.walletSmartStateDidUpdate() }
    }
    
    private func notifyBlacklistedCertificates() {
        observers.forEach { $0.observer?.walletBlacklistDidUpdate() }
    }
}

private extension EuropeanCertificate {
    func isProfileMismatchForLastname(to other: EuropeanCertificate) -> Bool {
        return birthdate == other.birthdate && lastname != other.lastname
    }
    
    func isProfileMismatchForFirstname(to other: EuropeanCertificate) -> Bool {
        return birthdate == other.birthdate && firstname != other.firstname
    }
    
    func isProfileMismatchForDateOfBirth(to other: EuropeanCertificate) -> Bool {
        return birthdate != other.birthdate && firstname == other.firstname && lastname == other.lastname
    }
}

// MARK: 
private extension WalletManager {
    func updateBlacklistWith(new certificate: WalletCertificate, queue: DispatchQueue) {
        // Do not check blacklist if certificate is expired
        if let europeanCertificate = certificate as? EuropeanCertificate, (isPassExpired(for: europeanCertificate) || europeanCertificate.isExpired) { return }
        
        queue.async { [weak self] in
            guard let self = self else { return }
            BlacklistManager.current.isCertificateBlacklisted(certificate, queue: queue) { isBlacklisted in
                DispatchQueue.main.async {
                    if let isBlacklisted = isBlacklisted {
                        let index: Int? = self.blacklistedCertificates.firstIndex { $0.uniqueHash == certificate.uniqueHash }
                        if let index = index, !isBlacklisted {
                            self.blacklistedCertificates.remove(at: index)
                            self.notifyBlacklistedCertificates()
                        } else if isBlacklisted {
                            self.blacklistedCertificates.append(certificate)
                            self.notifyBlacklistedCertificates()
                        }
                    } else {
                        self.blacklistIsDirty = true
                    }
                }
            }
        }
    }
    
    func updateWalletBlacklistedCertificates(queue: DispatchQueue) {
        let relevantCertificates: [WalletCertificate] = _walletCertificates.filter {
            if let europeanCertificate = $0 as? EuropeanCertificate {
                return !isPassExpired(for: europeanCertificate) && !europeanCertificate.isExpired
            } else {
                return true
            }
        }
        queue.async { [weak self] in
            guard let self = self else { return }
            BlacklistManager.current.blacklistedCertificates(in: relevantCertificates, forceReload: self.blacklistIsDirty, queue: queue) { success, blacklistedCertificates in
                DispatchQueue.main.async {
                    if success { self.blacklistIsDirty = false }
                    self.blacklistedCertificates = blacklistedCertificates ?? []
                    self.notifyBlacklistedCertificates()
                }
            }
        }
    }
}

extension WalletManager: LocalizationsChangesObserver {
    func localizationsChanged() {
        _walletCertificates.forEach { ($0 as? EuropeanCertificate)?.languageReset() }
    }
}
