// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
//
//  EuropeanCertificate.swift
//  TousAntiCovid
//
//  Created by Lunabee Studio / Date - 25/05/2021 - for the TousAntiCovid project.
//

import UIKit
import ServerSDK
import StorageSDK

class EuropeanCertificate: WalletCertificate, Hashable {
    
    var authority: String?
    var certificateId: String?

    var id: String
    var type: WalletConstant.CertificateType
    var value: String
    var message: Data?
    var signature: Data?

    let hCert: HCert
    
    var isFromMultiPassIssuer: Bool { hCert.uvci.starts(with: "URN:UVCI:01:FR:DGSAG/") }

    lazy var fullName: String =
        [firstname, lastname].compactMap { $0?.trimmingCharacters(in: .whitespaces) }.joined(separator: " ")

    lazy var firstFirstName: String = hCert.get(.firstName).string.nilIfEmpty?.components(separatedBy: CharacterSet(charactersIn: " -")).first ?? hCert.get(.lastNameStandardized).string?.replacingOccurrences(of: "\\<+$", with: "", options: [.regularExpression]) ?? ""

    lazy var hCertPrefix: String? = {
        switch type {
        case .recoveryEurope:
            return hCert.recoveryStatements.first?.issuer
        case .sanitaryEurope:
            return hCert.testStatements.first?.testCenter
        default: return nil
        }
    }()

    lazy var timestamp: Double = {
        var date: Date?
        switch hCert.certificateType {
        case .vaccine:
            date = hCert.vaccineStatements.first?.date
        case .test:
            date = hCert.testStatements.first?.sampleTime
        case .recovery:
            date = Date(dateString: hCert.recoveryStatements.first?.firstPositiveDate ?? "")
        case .exemption:
            date = hCert.exemptionStatement?.validFrom
        case .unknown:
            break
        }
        return (date ?? hCert.iat).timeIntervalSince1970
    }()
    
    var pillTitles: [(text: String, backgroundColor: UIColor)] {
        if _pillTitles == nil {
            var pills: [(String, UIColor)] = [(hCert.certTypeString.trimmingCharacters(in: .whitespaces), Appearance.tintColor)]
            if isFromMultiPassIssuer { pills.append(("wallet.tag.multiPass".localized, Asset.Colors.leather.color)) }
            if isExpired { pills.append(("wallet.expired.pillTitle".localized, Asset.Colors.error.color)) }
            _pillTitles = pills
        }
        return _pillTitles ?? []
    }

    var title: String? {
        if _title == nil {
            let flag: String? = isForeignCertificate ? countryCode?.flag() : nil
            switch hCert.certificateType {
            case .vaccine:
                _title = [flag, "wallet.proof.europe.vaccine.title".localized].compactMap { $0 }.joined(separator: " ")
            case .test:
                _title = [flag, "wallet.proof.europe.test.title".localized].compactMap { $0 }.joined(separator: " ")
            case .recovery:
                _title = [flag, "wallet.proof.europe.recovery.title".localized].compactMap { $0 }.joined(separator: " ")
            case .exemption:
                _title = [flag, "wallet.proof.europe.exemption.title".localized].compactMap { $0 }.joined(separator: " ")
            case .unknown:
                _title = nil
            }
        }
        if type == .multiPass {
            _title = "wallet.tag.title".localized
        }
        return _title
    }

    lazy var shortDescriptionForList: String? = [smiley, fullName].compactMap { $0 }.joined(separator: " ")

    lazy var shortDescription: String? = fullName

    var fullDescription: String? {
        if _fullDescription == nil {
            var strings: [String?] = []
            switch hCert.certificateType {
            case .vaccine:
                strings.append(fullDescriptionVaccination())
            case .test:
                strings.append(fullDescriptionTest(forceEnglishFormat: false))
            case .recovery:
                strings.append(fullDescriptionRecovery(forceEnglishFormat: false))
            case .exemption:
                strings.append(fullDescriptionExemption(forceEnglishFormat: false))
            case .unknown:
                break
            }
            _fullDescription = strings.compactMap { $0 }.joined(separator: "\n\n")
        }
        return _fullDescription
    }

    lazy var smiley: String? = {
        guard let dccKids = ParametersManager.shared.dccKids, userAge <= dccKids.age else { return nil }
        let emojiIdx: Int = (hCert.dateOfBirth + fullName).androidCommonHash() % dccKids.smileys.count
        return dccKids.smileys[emojiIdx]
    }()

    lazy var smartWalletProfileId: String = (firstname ?? lastname ?? "").uppercased() + hCert.dateOfBirth
    lazy var multiPassProfileId: String = firstFirstName.uppercased() + hCert.dateOfBirth

    lazy var uniqueHash: String = "\(countryCode?.uppercased() ?? "")\(hCert.uvci)".sha256()

    var additionalInfo: [AdditionalInfo] {
        var info: [AdditionalInfo] = getAdditionalInfo()
        if let foreignWarning = "wallet.proof.europe.foreignCountryWarning.\(countryCode?.lowercased() ?? "")".localizedOrNil {
            info.append(AdditionalInfo(category: .info, fullDescription: foreignWarning))
        }
        if isNegativeTest == false && type == .sanitaryEurope {
            info.append(AdditionalInfo(category: .warning, fullDescription: "wallet.proof.europe.test.positiveSidepError".localized))
        }
        if isAutoTest {
            info.append(AdditionalInfo(category: .warning, fullDescription: "wallet.autotest.warning".localized))
        }
        return info
    }

    lazy var dosesNumber: Int? = hCert.vaccineStatements.first?.doseNumber
    lazy var dosesTotal: Int? = hCert.vaccineStatements.first?.dosesTotal
    
    lazy var isAntigenTest: Bool = { hCert.testStatements.first?.type == WalletConstant.TestType.antigen.rawValue }()

    lazy var isForeignCertificate: Bool = countryCode != "FR"
    lazy var technicalExpirationDate: Date? = {
        switch hCert.certificateType {
        case .exemption:
            return hCert.exemptionStatement?.validUntil
        default:
            return hCert.exp
        }
    }()
    var isExpired: Bool {
        (technicalExpirationDate ?? .distantPast).timeIntervalSince1970 < Date().timeIntervalSince1970
    }
    lazy var kid: String = hCert.kidStr

    var fullDescriptionForFullscreen: String? {
        if _fullDescriptionForFullscreen == nil {
            switch hCert.certificateType {
            case .vaccine:
                _fullDescriptionForFullscreen = borderDescriptionVaccination()
            case .test:
                _fullDescriptionForFullscreen = fullDescriptionTest(forceEnglishFormat: true)
            case .recovery:
                _fullDescriptionForFullscreen = fullDescriptionRecovery(forceEnglishFormat: true)
            case .exemption:
                _fullDescriptionForFullscreen = fullDescriptionExemption(forceEnglishFormat: true)
            case .unknown:
                _fullDescriptionForFullscreen = nil
            }
        }
        return _fullDescriptionForFullscreen
    }

    var vaccineType: WalletConstant.VaccineType? {
        guard let medicalProductCode = medicalProductCode else { return nil }
        var vaccineType: WalletConstant.VaccineType?
        WalletConstant.VaccineType.allCases.forEach { type in
            if type.stringValues().contains(medicalProductCode) { vaccineType = type }
        }
        return vaccineType
    }

    lazy var medicalProductCode: String? = {
        guard let vaccinationEntry = hCert.vaccineStatements.first else { return nil }
        return vaccinationEntry.medicalProduct.trimmingCharacters(in: .whitespaces)
    }()

    lazy var firstname: String? = hCert.get(.firstName).string ?? hCert.get(.firstNameStandardized).string
    lazy var lastname: String? = hCert.get(.lastName).string ?? hCert.get(.lastNameStandardized).string
    lazy var birthdate: Double = Date(dateString: hCert.dateOfBirth)?.timeIntervalSince1970 ?? Date().timeIntervalSince1970
    lazy var hasLunarBirthdate: Bool = hCert.dateOfBirth.isLunarDate

    lazy var isLastDose: Bool? = {
        guard let dosesNumber = dosesNumber, let dosesTotal = dosesTotal else { return false }
        return dosesNumber >= dosesTotal
    }()

    var isValidNegativeTest: Bool? {
        guard let isNegativeTest = isNegativeTest else { return nil }
        return isNegativeTest && !isTestCertificateTooOld()
    }

    lazy var isNegativeTest: Bool? = hCert.testStatements.first?.resultNegative

    // In order to improve performance, static strings are set only once and reset only when the local language is changed
    private var _fullDescription: String?
    private var _fullDescriptionForFullscreen: String?
    private var _pillTitles: [(text: String, backgroundColor: UIColor)]?
    private var _title: String?

    init(id: String, value: String, type: WalletConstant.CertificateType, hCert: HCert) {
        self.hCert = hCert
        self.id = id
        self.value = value
        self.type = type
    }

    func toRawCertificate() -> RawWalletCertificate {
        RawWalletCertificate(id: id, value: value, expiryDate: hCert.exp)
    }

    var vaccineValidityLocalizedDescription: String? {
        guard !isExpired else {
            if let date = technicalExpirationDate {
                return String(format: "europeanCertificate.fullscreen.expirationDate".localized, date.dayMonthYearFormatted())
            } else { return nil }
        }
        var desc: String = "smartwallet.vaccine.valid.none".localized
        guard let validity: ValidityInfo = WalletManager.shared.validity(self) else { return desc }
        if let end = validity.endTimestamp, end < Date().timeIntervalSince1970 {
            desc = "smartwallet.vaccine.valid.none".localized
        } else if let start = validity.startTimestamp, let end = validity.endTimestamp {
            desc = "smartwallet.vaccine.valid.startend".localized
                .replacingOccurrences(of: "<FROM_DATE>", with: Date(timeIntervalSince1970: start).dayShortMonthYearFormatted(timeZoneIndependant: true))
                .replacingOccurrences(of: "<TO_DATE>", with: Date(timeIntervalSince1970: end).dayShortMonthYearFormatted(timeZoneIndependant: true))
        } else if let end = validity.endTimestamp {
            desc = "smartwallet.vaccine.valid.end".localized
                .replacingOccurrences(of: "<TO_DATE>", with: Date(timeIntervalSince1970: end).dayShortMonthYearFormatted(timeZoneIndependant: true))
        } else if let start = validity.startTimestamp {
            desc = "smartwallet.vaccine.valid.start".localized
                .replacingOccurrences(of: "<FROM_DATE>", with: Date(timeIntervalSince1970: start).dayShortMonthYearFormatted(timeZoneIndependant: true))
        }
        return desc
    }

    var recoveryValidityLocalizedDescription: String? {
        guard !isExpired else {
            if let date = technicalExpirationDate {
                return String(format: "europeanCertificate.fullscreen.expirationDate".localized, date.dayMonthYearFormatted())
            } else { return nil }
        }
        var desc: String = "smartwallet.recovery.valid.none".localized
        guard let validity: ValidityInfo = WalletManager.shared.validity(self) else { return desc }
        if let end = validity.endTimestamp, end < Date().timeIntervalSince1970 {
            desc = "smartwallet.recovery.valid.none".localized
        } else if let start = validity.startTimestamp, let end = validity.endTimestamp {
            desc = "smartwallet.recovery.valid.startend".localized
                .replacingOccurrences(of: "<FROM_DATE>", with: Date(timeIntervalSince1970: start).dayShortMonthYearFormatted(timeZoneIndependant: true))
                .replacingOccurrences(of: "<TO_DATE>", with:  Date(timeIntervalSince1970: end).dayShortMonthYearFormatted(timeZoneIndependant: true))
        }
        else if let end = validity.endTimestamp {
            desc = "smartwallet.recovery.valid.end".localized
                .replacingOccurrences(of: "<TO_DATE>", with:  Date(timeIntervalSince1970: end).dayShortMonthYearFormatted(timeZoneIndependant: true))
        }
        else if let start = validity.startTimestamp {
            desc = "smartwallet.recovery.valid.start".localized
                .replacingOccurrences(of: "<FROM_DATE>", with: Date(timeIntervalSince1970: start).dayShortMonthYearFormatted(timeZoneIndependant: true))
        }
        return desc
    }

    var positiveTestValidityLocalizedDescription: String? {
        guard !isExpired else {
            if let date = technicalExpirationDate {
                return String(format: "europeanCertificate.fullscreen.expirationDate".localized, date.dayMonthYearFormatted())
            } else { return nil }
        }
        var desc: String = "smartwallet.positiveTest.valid.none".localized
        guard let validity: ValidityInfo = WalletManager.shared.validity(self) else { return desc }
        if let end = validity.endTimestamp, end < Date().timeIntervalSince1970 {
            desc = "smartwallet.positiveTest.valid.none".localized
        } else if let start = validity.startTimestamp, let end = validity.endTimestamp {
            desc = "smartwallet.positiveTest.valid.startend".localized
                .replacingOccurrences(of: "<FROM_DATE>", with: Date(timeIntervalSince1970: start).dayShortMonthYearFormatted(timeZoneIndependant: true))
                .replacingOccurrences(of: "<TO_DATE>", with:  Date(timeIntervalSince1970: end).dayShortMonthYearFormatted(timeZoneIndependant: true))
        }
        else if let end = validity.endTimestamp {
            desc = "smartwallet.positiveTest.valid.end".localized
                .replacingOccurrences(of: "<TO_DATE>", with:  Date(timeIntervalSince1970: end).dayShortMonthYearFormatted(timeZoneIndependant: true))
        }
        else if let start = validity.startTimestamp {
            desc = "smartwallet.positiveTest.valid.start".localized
                .replacingOccurrences(of: "<FROM_DATE>", with: Date(timeIntervalSince1970: start).dayShortMonthYearFormatted(timeZoneIndependant: true))
        }
        return desc
    }

    private lazy var countryCode: String? = {
        let countryCode: String?
        switch hCert.certificateType {
        case .vaccine:
            countryCode = hCert.vaccineStatements.first?.countryCode
        case .test:
            countryCode = hCert.testStatements.first?.countryCode
        case .recovery:
            countryCode = hCert.recoveryStatements.first?.countryCode
        case .exemption:
            countryCode = hCert.exemptionStatement?.countryCode
        case .unknown:
            countryCode = nil
        }
        return ["NC", "WF", "PM", "PF"].contains(countryCode) ? "FR" : countryCode
    }()

    private lazy var isAutoTest: Bool = hCert.testStatements.first?.manufacturer?.lowercased() == "autotest"
    
    static func == (lhs: EuropeanCertificate, rhs: EuropeanCertificate) -> Bool {
        lhs.value == rhs.value
    }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(value)
    }
    
    func languageReset() {
        _title = nil
        _pillTitles = nil
        _fullDescription = nil
        _fullDescriptionForFullscreen = nil
    }

    private func birthDateString(forceEnglishFormat: Bool) -> String? {
        Date(dateString: hCert.dateOfBirth)?.dayShortMonthYearFormatted(timeZoneIndependant: true, forceEnglishFormat: forceEnglishFormat)
    }

    private func fullDescriptionVaccination() -> String? {
        // Still waiting for string format confirmation.
        guard let vaccinationEntry = hCert.vaccineStatements.first else { return nil }

        let info: String = "wallet.proof.europe.vaccine.identity".localized
        let date: String = vaccinationEntry.date.dayShortMonthYearFormatted(timeZoneIndependant: true)
        return [info
                    .replacingOccurrences(of: "<BIRTHDATE>", with: birthDateString(forceEnglishFormat: false) ?? "")
                    .replacingOccurrences(of: "<VACCINE_NAME>", with: l10n("vac.product.\(medicalProductCode ?? "")", or: medicalProductCode))
                    .replacingOccurrences(of: "<DATE>", with: date), vaccineValidityLocalizedDescription].compactMap { $0 }.joined(separator: "\n")
    }

    private func borderDescriptionVaccination() -> String? {
        // Still waiting for string format confirmation.
        guard let vaccinationEntry = hCert.vaccineStatements.first else { return nil }

        let flag: String? = isForeignCertificate ? countryCode?.flag() : nil
        let info: String = "europeanCertificate.fullscreen.englishDescription.vaccine".localized
        let date: String = vaccinationEntry.date.dayShortMonthYearFormatted(timeZoneIndependant: true, forceEnglishFormat: true)
        return [flag, info].compactMap { $0 }.joined(separator: " ")
            .replacingOccurrences(of: "<FULL_NAME>", with: fullName)
            .replacingOccurrences(of: "<BIRTHDATE>", with: birthDateString(forceEnglishFormat: true) ?? "")
            .replacingOccurrences(of: "<VACCINE_NAME>", with: l10n("vac.product.\(medicalProductCode ?? "")", or: medicalProductCode))
            .replacingOccurrences(of: "<VACCINE_DOSES>", with: "\(hCert.vaccineStatements.first?.doseNumber ?? 0)/\(hCert.vaccineStatements.first?.dosesTotal ?? 0)")
            .replacingOccurrences(of: "<DATE>", with: date)
    }

    private func fullDescriptionTest(forceEnglishFormat: Bool) -> String? {
        guard let testEntry = hCert.testStatements.first else { return nil }
        let testResultKey: String = isNegativeTest == true ? "negative" : "positive"
        let flag: String? = isForeignCertificate && forceEnglishFormat ? countryCode?.flag() : nil
        let info: String = forceEnglishFormat ? "europeanCertificate.fullscreen.englishDescription.test".localized : "wallet.proof.europe.test.identity".localized
        let sampleTimeDate: Date = testEntry.sampleTime
        let date: String = sampleTimeDate.dayShortMonthYearTimeFormatted(forceEnglishFormat: forceEnglishFormat)
        let result: String = forceEnglishFormat ? "wallet.proof.europe.test.englishDescription.\(testResultKey)".localized : "wallet.proof.europe.test.\(testResultKey)".localized
        let analysisCode: String = forceEnglishFormat ? l10n("test.man.englishDescription.\(testEntry.type)", or: testEntry.type) : l10n("test.man.\(testEntry.type)", or: testEntry.type)

        if isNegativeTest == false, !forceEnglishFormat {
            return [[flag, info].compactMap { $0 } .joined(separator: " ")
                        .replacingOccurrences(of: "<BIRTHDATE>", with: birthDateString(forceEnglishFormat: forceEnglishFormat) ?? "")
                        .replacingOccurrences(of: "<ANALYSIS_CODE>", with: analysisCode)
                        .replacingOccurrences(of: "<ANALYSIS_RESULT>", with: result)
                        .replacingOccurrences(of: "<DATE>", with: date), positiveTestValidityLocalizedDescription].compactMap { $0 }.joined(separator: "\n")
        } else {
            return [flag, info].compactMap { $0 } .joined(separator: " ")
                .replacingOccurrences(of: "<FULL_NAME>", with: fullName)
                .replacingOccurrences(of: "<BIRTHDATE>", with: birthDateString(forceEnglishFormat: forceEnglishFormat) ?? "")
                .replacingOccurrences(of: "<ANALYSIS_CODE>", with: analysisCode)
                .replacingOccurrences(of: "<ANALYSIS_RESULT>", with: result)
                .replacingOccurrences(of: "<DATE>", with: date)
                .appending(isNegativeTest == true ? "\n\(validityString(forceEnglish: forceEnglishFormat))" : "")
        }
    }

    private func fullDescriptionRecovery(forceEnglishFormat: Bool) -> String? {
        guard let recoveryEntry = hCert.recoveryStatements.first else { return nil }
        let firstPositiveDate: Date? = Date(dateString: recoveryEntry.firstPositiveDate)
        let flag: String? = isForeignCertificate && forceEnglishFormat ? countryCode?.flag() : nil
        let info: String = forceEnglishFormat ? "europeanCertificate.fullscreen.englishDescription.recovery".localized : "wallet.proof.europe.recovery.identity".localized
        let date: String = firstPositiveDate?.dayShortMonthYearFormatted(timeZoneIndependant: true, forceEnglishFormat: forceEnglishFormat) ?? ""

        if forceEnglishFormat {
            return [flag, info].compactMap { $0 } .joined(separator: " ")
                .replacingOccurrences(of: "<FULL_NAME>", with: fullName)
                .replacingOccurrences(of: "<BIRTHDATE>", with: birthDateString(forceEnglishFormat: forceEnglishFormat) ?? "")
                .replacingOccurrences(of: "<DATE>", with: date)
        } else {
            return [[flag, info].compactMap { $0 } .joined(separator: " ")
                        .replacingOccurrences(of: "<BIRTHDATE>", with: birthDateString(forceEnglishFormat: forceEnglishFormat) ?? "")
                        .replacingOccurrences(of: "<DATE>", with: date), recoveryValidityLocalizedDescription].compactMap { $0 }.joined(separator: "\n")
        }
    }

    private func fullDescriptionExemption(forceEnglishFormat: Bool) -> String? {
        guard let exemptionEntry = hCert.exemptionStatement else { return nil }
        let flag: String? = isForeignCertificate && forceEnglishFormat ? countryCode?.flag() : nil
        let info: String = forceEnglishFormat ? "europeanCertificate.fullscreen.englishDescription.exemption".localized : "wallet.proof.europe.exemption.identity".localized
        let dateFromStr: String = exemptionEntry.validFrom.dayShortMonthYearFormatted(timeZoneIndependant: true, forceEnglishFormat: forceEnglishFormat)
        let dateUntilStr: String = exemptionEntry.validUntil.dayShortMonthYearFormatted(timeZoneIndependant: true, forceEnglishFormat: forceEnglishFormat)
        if forceEnglishFormat {
            return [flag, info].compactMap { $0 } .joined(separator: " ")
                .replacingOccurrences(of: "<FULL_NAME>", with: fullName)
                .replacingOccurrences(of: "<BIRTHDATE>", with: birthDateString(forceEnglishFormat: forceEnglishFormat) ?? "")
                .replacingOccurrences(of: "<FROM_DATE>", with: dateFromStr)
                .replacingOccurrences(of: "<TO_DATE>", with: dateUntilStr)
        } else {
            return [[flag, info].compactMap { $0 } .joined(separator: " ")
                        .replacingOccurrences(of: "<BIRTHDATE>", with: birthDateString(forceEnglishFormat: false) ?? ""), exemptionValidityLocalizedDescription]
                .compactMap { $0 }
                .joined(separator: "\n")
        }
    }

    private func isTestCertificateTooOld() -> Bool {
        guard hCert.testStatements.first?.resultNegative == true else { return false }
        let durationThresholdForEligiblility: Double = Double(ParametersManager.shared.activityPassSkipNegTestHours) * 3600.0
        return timestamp + durationThresholdForEligiblility < Date().timeIntervalSince1970
    }

    private var exemptionValidityLocalizedDescription: String? {
        guard let validity: ValidityInfo = WalletManager.shared.validity(self) else { return nil }
        var desc: String?
        if let start = validity.startTimestamp, let end = validity.endTimestamp {
            desc = "smartwallet.recovery.valid.startend".localized
                .replacingOccurrences(of: "<FROM_DATE>", with: Date(timeIntervalSince1970: start).dayShortMonthYearFormatted(timeZoneIndependant: true))
                .replacingOccurrences(of: "<TO_DATE>", with:  Date(timeIntervalSince1970: end).dayShortMonthYearFormatted(timeZoneIndependant: true))
        }
        return desc
    }
}
