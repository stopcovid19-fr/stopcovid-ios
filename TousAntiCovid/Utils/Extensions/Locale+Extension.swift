// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
//
//  Locale+Extension.swift
//  TousAntiCovid
//
//  Created by Lunabee Studio / Date - 08/04/2020 - for the TousAntiCovid project.
//

import UIKit
import UseCases

extension Locale {
    
    static var appLocale: Locale { .init(identifier: Locale.currentAppLanguageCode) }

    static var currentAppLanguageCode: String {
        UseCases.getCurrentAppLanguage(localeLanguageCode: Locale.current.languageCode.orEmpty)?.rawValue ?? UseCases.defaultAppLanguage.rawValue
    }

}
