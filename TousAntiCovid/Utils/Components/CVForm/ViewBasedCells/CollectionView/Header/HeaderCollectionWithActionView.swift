// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
//
//  HeaderCollectionWithActionView.swift
//  TousAntiCovid
//
//  Created by Lunabee Studio / Date - 16/03/2022 - for the TousAntiCovid project.
//

import UIKit

final class HeaderCollectionWithActionView: CVCollectionFooterHeaderView {    
    @IBOutlet private weak var actionButton: UIButton!
    @IBOutlet weak var contentStackView: UIStackView!
    
    @IBAction private func buttonPressed() {
        buttonAction()
    }
    
    override func setup(with headerSection: CVFooterHeaderSection) {
        if let subtitle = headerSection.subtitle {
            let attributedSubtitle: NSAttributedString = .init(string: subtitle, attributes: [.font: headerSection.theme.subtitleFont()])
            actionButton.setAttributedTitle(attributedSubtitle, for: .normal)
        }
        super.setup(with: headerSection)
        switch headerSection.theme.axis {
        case .vertical:
            contentStackView.axis = .vertical
            contentStackView.alignment = .leading
        default:
            contentStackView.axis = .horizontal
            contentStackView.alignment = .fill
        }
        setupTheme()
    }
    
    override func setupAccessibility(isHeader: Bool) {
        super.setupAccessibility(isHeader: isHeader)
        if let buttonTitle = actionButton.attributedTitle(for: .normal)?.string, !buttonTitle.isEmpty {
            accessibilityCustomActions = [UIAccessibilityCustomAction(name: buttonTitle, target: self, selector: #selector(self.buttonAction))]
        }
    }
}

// MARK: - Private functions
private extension HeaderCollectionWithActionView {
    @discardableResult
    @objc func buttonAction() -> Bool {
        currentAssociatedHeaderSection?.selectionAction?()
        return true
    }
    
    func setupTheme() {
        actionButton.tintColor = Appearance.tintColor
    }
}
 
