// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
//
//  CVCollectionViewController.swift
//  TousAntiCovid
//
//  Created by Lunabee Studio / Date - 11/03/2022 - for the TousAntiCovid project.
//

import UIKit

@available(iOS 13.0, *)
extension UICollectionViewDiffableDataSourceReference {

    /// A convenience initialiser to set up a `UICollectionViewDiffableDataSourceReference` with existing data source methods. This makes it low effort to use diffable datasources while keeping existing code untouched.
    /// Eventually, when dropping iOS 12, you might want to revisit this code and remove the existing `UICollectionViewDataSource` implementations.
    ///
    /// - Parameters:
    ///   - collectionView: The collection view for which we're setting the data source
    ///   - collectionViewDataSource: The original existing collection view data source.
    convenience init(collectionViewController: UICollectionViewController) {
        self.init(collectionView: collectionViewController.collectionView) { [weak collectionViewController] (collectionView, indexPath, _) -> UICollectionViewCell? in
            return collectionViewController?.collectionView(collectionView, cellForItemAt: indexPath)
        }
        supplementaryViewProvider = { [weak collectionViewController] (collectionView, kind, indexPath) -> UICollectionReusableView? in
            return collectionViewController?.collectionView(collectionView, viewForSupplementaryElementOfKind: kind, at: indexPath)
        }
    }
}

@available(iOS 13.0, *)
class CVCollectionViewController: UICollectionViewController, ReloadableContentController {
    private var diffableDataSource: UICollectionViewDiffableDataSourceReference!
    
    var sections: [CVCollectionSection] = []
    
    func createSections() -> [CVCollectionSection] { [] }

    override init(collectionViewLayout layout: UICollectionViewLayout) {
        super.init(collectionViewLayout: layout)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        diffableDataSource = UICollectionViewDiffableDataSourceReference(collectionViewController: self)
        collectionView.dataSource = diffableDataSource
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func reloadUI(animated: Bool = false, animatedView: UIView? = nil, completion: (() -> ())? = nil) {
        sections = createSections()
        registerXibs()
        let snapshot = NSDiffableDataSourceSnapshotReference()
        snapshot.appendSections(withIdentifiers: sections.compactMap { $0.hashValue })
        sections.forEach { section in
            snapshot.appendItems(withIdentifiers: section.rows.compactMap { $0.hashValue }, intoSectionWithIdentifier: section.hashValue)
        }
        (collectionView.dataSource as? UICollectionViewDiffableDataSourceReference)?.applySnapshot(snapshot, animatingDifferences: false) {
            completion?()
        }
    }

    func addHeaderView(height: CGFloat = Appearance.TableView.Header.standardHeight) {
        let valueToSet: CGFloat = height + view.safeAreaInsets.top
        guard collectionView.contentInset.top != valueToSet else { return }
        collectionView.contentInset.top = valueToSet
    }

    func registerXibs() {
        let xibNames: Set<String> = Set<String>(sections.map { $0.rows.compactMap { $0.xibName.rawValue } }.reduce([], +))
        let headerSectionsXibNames: Set<String> = Set<String>(sections.compactMap { $0.header?.xibName.rawValue })
        xibNames.forEach {
            if $0 == XibName.Row.homeKeyFigureChartCollectionCell.rawValue {
                collectionView.register(UINib(nibName: $0, bundle: nil), forCellWithReuseIdentifier: $0)
            } else {
                collectionView.register(CVCollectionCell.self, forCellWithReuseIdentifier: $0)
            }
        }
        headerSectionsXibNames.forEach { collectionView.register(UINib(nibName: $0, bundle: nil), forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: $0) }
    }
    
    func sectionObject(at index: Int) -> CVCollectionSection { sections[index] }
    
    func rowObject(at indexPath: IndexPath) -> CVRow { sections[indexPath.section].rows[indexPath.row] }

    override func numberOfSections(in collectionView: UICollectionView) -> Int { sections.count }

    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        sectionObject(at: section).rows.count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let row: CVRow = rowObject(at: indexPath)
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: row.xibName.rawValue, for: indexPath) as? CVCollectionCell else { return UICollectionViewCell() }
        cell.setup(with: row)
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let section: CVCollectionSection = sectionObject(at: indexPath.section)
        guard let supplementary = section.supplementarySection(for: kind) else { return UICollectionReusableView() }
        guard let view = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: supplementary.xibName.rawValue, for: indexPath) as? CVCollectionFooterHeaderView else { return UICollectionReusableView() }
        view.setup(with: supplementary)
        return view
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let row: CVRow = rowObject(at: indexPath)
        row.selectionAction?(nil)
    }
    
    override func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        let row: CVRow = rowObject(at: indexPath)
        if let cell = (cell as? CVCollectionCell)?.contentCell {
            row.willDisplay?(cell)
        } else { // Special case for lottie animation cell
            row.willDisplay?(cell)
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, willDisplaySupplementaryView view: UICollectionReusableView, forElementKind elementKind: String, at indexPath: IndexPath) {
        let section: CVCollectionSection = sectionObject(at: indexPath.section)
        guard let view = view as? CVCollectionFooterHeaderView else { return }
        section.willDisplay(for: elementKind, view: view)
    }
}
