// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
//
//  CVCardCell.swift
//  TousAntiCovid
//
//  Created by Lunabee Studio / Date - 15/03/2022 - for the TousAntiCovid project.
//

import UIKit

class CVCardCell: CVCell {
    @IBOutlet var containerView: UIView!
    
    override func setup(with row: CVRow) {
        super.setup(with: row)
        containerView.layer.cornerRadius = 10.0
        containerView.layer.masksToBounds = true
        containerView.backgroundColor = backgroundColor
        containerView.layer.maskedCorners = row.theme.maskedCorners
        backgroundColor = .clear
    }

    override func setHighlighted(_ highlighted: Bool) {
        guard currentAssociatedRow?.selectionAction != nil else { return }
        super.setHighlighted(highlighted)
        if highlighted {
            layer.removeAllAnimations()
            alpha = 0.6
        } else {
            UIView.animate(withDuration: 0.3) {
                self.alpha = 1.0
            }
        }
    }

    override func setupAccessibility() {
        let row: CVRow? = currentAssociatedRow
        accessibilityLabel = row?.title?.removingEmojis()
        accessibilityHint = [row?.subtitle, row?.accessoryText].compactMap { $0?.accessibilityNumberFormattedString() }.joined(separator: ".\n").removingEmojis()
        accessibilityTraits = row?.selectionAction != nil ? .button : .staticText
        accessibilityElements = []
        isAccessibilityElement = true
        containerView.isAccessibilityElement = false
        cvAccessoryLabel?.isAccessibilityElement = false
        cvTitleLabel?.isAccessibilityElement = false
        cvSubtitleLabel?.isAccessibilityElement = false
        cvImageView?.isAccessibilityElement = false
        cvImageView?.accessibilityTraits = []
        cvImageView?.isUserInteractionEnabled = false
    }
}
